# Nuke

#### Description
Nuke Animate Test

#### Software Architecture
Software architecture description

#### Instructions

#### Postscript
插件安装到C:\Users\FPG\.nuke这个目录下。

1. Linux: /usr/local/NukeX.XvX/python
2. MacOSX: /Applications/NukeX.XvX/NukeX.XvX.app/Contents/MacOS/python
3. Windows: C:\Program Files\NukeX.XvX\python.exe
4. 第一种运行方式：可用PyCharm编辑器直接运行，在建项目的时候，选择nuke下面的python解释器(路径如上所述)
5. 第二种运行方式：在以上的路径下直接运行main.py文件(例如在Windows系统下运行命令: C:\Program Files\Nuke11.2v3> .\python E:\pythonProject\nuke\main.py)
6. C:\Program Files\Nuke11.2v3> ./Nuke11.2 -t E:\pythonProject\Nuke\Text3\main2.py