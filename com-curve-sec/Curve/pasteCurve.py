import math

if tn.knob("sourceText").value() == "ok" and tn.knob("targetText").value() == "ok":
    sNodeName = tn.knob("sourceNodes").value()
    sAttrMultipleName = tn.knob("attributeMultiple").value()
    sAttrSingleName = tn.knob("attributeSingle").value()
    sAttrSingleNames = tn.knob("attributeSingle").values()
    sFirstFrame = tn.knob("sourceFirstFrameX").value()
    sLastFrame = tn.knob("sourceLastFrameX").value()
    sFirstFrameY = tn.knob("sourceFirstFrameY").value()
    sLastFrameY = tn.knob("sourceLastFrameY").value()
    sDistance = sLastFrameY - sFirstFrameY
    sFrameCount = sLastFrame - sFirstFrame + 1

    curveAccuracy = float(tn.knob("CurveAccuracy").value()) * 1.0
    stepAccuracy = float(tn.knob("StepAccuracy").value()) * 1.0
    if curveAccuracy <= 0:
        curveAccuracy = 1.0
    if stepAccuracy <= 0:
        stepAccuracy = 1.0

    sIndexArr = [i for i, name in enumerate(sAttrSingleNames) if name == sAttrSingleName]
    sIndex = 0
    if len(sAttrSingleNames) > 1:
        sIndex = sIndexArr[0] - 1
    sAnimationCurve = nuke.toNode(sNodeName).knob(sAttrMultipleName).animations()[sIndex]

    isExpression = not sAnimationCurve.noExpression()

    xArr = []
    yArr = []

    flag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) > sAnimationCurve.evaluate(sFirstFrame + 0):
        flag = False
    else:
        flag = True

    equalFlag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) == sAnimationCurve.evaluate(sFirstFrame + 0):
        equalFlag = True
    else:
        equalFlag = False

    xArr.append(sFirstFrame)
    yArr.append(sAnimationCurve.evaluate(sFirstFrame))

    for i in range(int((sLastFrame - sFirstFrame) * curveAccuracy)):
        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) == 0:
            if not equalFlag:
                xArr.append(sFirstFrame + i / curveAccuracy)
                yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            equalFlag = True
        else:
            if equalFlag:
                if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) > sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy):
                    flag = False
                else:
                    flag = True
            equalFlag = False

        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) > 0:
            if not flag:
                if i / curveAccuracy != 0:
                    xArr.append(sFirstFrame + i / curveAccuracy)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            flag = True
        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) < 0:
            if flag:
                if i / curveAccuracy != 0:
                    xArr.append(sFirstFrame + i / curveAccuracy)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            flag = False

    xArr.append(sLastFrame)
    yArr.append(sAnimationCurve.evaluate(sLastFrame))

    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()
    tFirstFrame = tn.knob("targetFirstFrameX").value()
    tLastFrame = tn.knob("targetLastFrameX").value()
    tFirstFrameY = tn.knob("targetFirstFrameY").value()
    tLastFrameY = tn.knob("targetLastFrameY").value()
    coverAll = tn.knob("CoverAll").value()
    tFrameCount = tLastFrame - tFirstFrame + 1

    tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
    tIndex = 0
    if len(tAttrSingleNames) > 1:
        tIndex = tIndexArr[0] - 1
    tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)
    # tAnimation.clearAnimated(tIndex)
    tAnimation.setAnimated(tIndex)

    tAnimationCurves = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()
    tAnimationCurve = None
    for j in range(len(tAnimationCurves)):
        if tAnimationCurves[j].knobIndex() == tIndex:
            tAnimationCurve = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()[j]

    keysLen = len(tAnimationCurve.keys())
    isLess = keysLen > 0 and tAnimationCurve.keys()[keysLen - 1].x <= tLastFrame

    removeKeys = []
    for key in tAnimationCurve.keys():
        if isLess:
            if key.x >= tFirstFrame and key.x <= tLastFrame:
                removeKeys.append(key)
        else:
            if key.x >= tFirstFrame and key.x < tLastFrame:
                removeKeys.append(key)
    tAnimationCurve.removeKey(removeKeys)


    # if not coverAll:
    #     tAnimation.clearAnimated(tIndex)
    #     tAnimation.setAnimated(tIndex)

    frameMultiple = (sFrameCount - 0) / (tFrameCount - 0) * 1.0
    frameMultiple2 = (sFrameCount - 1) / (tFrameCount - 1) * 1.0

    tYArr = []
    tXArr = []

    for i in range(len(xArr) - 1):
        if i == 0:
            tXArr.append(tFirstFrame)
        else:
            tXArr.append((xArr[i] - xArr[0]) / (sFrameCount - 1) * (tFrameCount - 1) + tFirstFrame)
    tXArr.append(tLastFrame)


    tDistance = tLastFrameY - tFirstFrameY
    stDistance = tDistance / sDistance * 1.0

    for j in range(len(yArr)):
        tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstFrameY)

    # if tAnimation.isAnimated():
    #     tAnimationCurve.clear()
    # tAnimation.deleteAnimation(nuke.toNode("Transform1").knob("translate").animations()[1])

    for k in range(len(tYArr) - 1):
        frameRotio = (xArr[k + 1] - xArr[k]) / (tXArr[k + 1] - tXArr[k])
        for i in range(int(math.ceil((tXArr[k + 1] - tXArr[k]) * stepAccuracy))):
            if yArr[k + 1] - yArr[k] == 0:
                #tAnimationCurve.setKey(xArr[k] / frameMultiple + i, tYArr[k])
                # tAnimation.setValueAt(tYArr[k], xArr[k] / frameMultiple + i, tIndex)
                tAnimation.setValueAt(tYArr[k], tXArr[k] + i / stepAccuracy, tIndex)
            else:
                frameX = tXArr[k] + i / stepAccuracy
                tAnimation.setValueAt( (sAnimationCurve.evaluate(xArr[k] + i / stepAccuracy * frameRotio) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k], frameX, tIndex)

    tAnimation.setValueAt(tLastFrameY, tLastFrame, tIndex)

    if isLess:
        if not isExpression:
            dir = -1
            if (sLastFrameY > sFirstFrameY and tLastFrameY > tFirstFrameY) or (
                    sLastFrameY < sFirstFrameY and tLastFrameY < tFirstFrameY):
                dir = 1
            sKeysLen = len(sAnimationCurve.keys())
            tKeysLen = len(tAnimationCurve.keys())
            tAnimationCurve.keys()[tKeysLen - 1].la = sAnimationCurve.keys()[sKeysLen - 1].la * dir
            tAnimationCurve.keys()[tKeysLen - 1].lslope = sAnimationCurve.keys()[sKeysLen - 1].lslope * dir
            tAnimationCurve.keys()[tKeysLen - 1].ra = sAnimationCurve.keys()[sKeysLen - 1].ra * dir
            tAnimationCurve.keys()[tKeysLen - 1].rslope = sAnimationCurve.keys()[sKeysLen - 1].rslope * dir



