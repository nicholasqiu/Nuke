#nuke.thisNode().knob("updateUI").setValue('''

tn = nuke.thisNode()
tk = nuke.thisKnob()
allNodes = nuke.root().nodes()

def setEnabled():
    tn.knob("sourceFirstFrameX").setEnabled(False)
    tn.knob("sourceLastFrameX").setEnabled(False)
    tn.knob("sourceFirstFrameY").setEnabled(False)
    tn.knob("sourceLastFrameY").setEnabled(False)

oldSourceNodes = tn.knob("sourceNodes").values()
sourceNodes = []

for node in allNodes:
    sourceNodes.append(node.name())

sourceNodes.sort()
sourceNodes.insert(0, "None")

ots = list(set(oldSourceNodes).difference(set(sourceNodes)))
sto = list(set(sourceNodes).difference(set(oldSourceNodes)))

setEnabled()

if len(ots) > 0 or len(sto) > 0:
    tn.knob("sourceNodes").setValues(sourceNodes)
    tn.knob("targetNodes").setValues(sourceNodes)
    tn.knob("EaseTargetNodes").setValues(sourceNodes)


#''')


