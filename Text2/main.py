import os
os.environ["NUKE_INTERACTIVE"] = "1"

import nuke


nuke.root()["last_frame"].setValue(100)
nuke.root()["format"].setValue("1K_Super_35(full-ap)")

#textNode = nuke.nodes.Text2(name="Text1")
textNode = nuke.createNode("Text2")
#textNode["size"].setValue(100)
#textNode["font"].setValue("C:/Windows/Fonts/arial.ttf")
textNode["message"].setValue("Fancy")
#textNode["xjustify"].setValue("left")
#textNode["yjustify"].setValue("bottom")

def test():
    print 123
    print textNode["box"].getValue()

nuke.addUpdateUI(test, nodeClass="*")
nuke.addOnUserCreate(test, nodeClass="*")
nuke.addOnScriptLoad(test, nodeClass="Root")
nuke.addOnCreate(test, nodeClass="*")
nuke.addKnobChanged(test, nodeClass="*", node=None)
nuke.addAfterBackgroundRender(test)
nuke.addAfterBackgroundFrameRender(test)

transformNode = nuke.nodes.Transform(name="Transform1")
#kTransform = transformNode["translate"]
#kTransform.setAnimated()
#kTransform.setValueAt(1024, 0, 0)
#kTransform.setValueAt(778, 0, 1)

transformNode.setInput(0, textNode)

print textNode["box"].getValue()

#nuke.toNode("Viewer1").setInput(0, transformNode)



writeNode = nuke.nodes.Write(name="Write1", file="C:/Users/FPG/Desktop/1.mov", file_type="mov")
writeNode.setInput(0, transformNode)
