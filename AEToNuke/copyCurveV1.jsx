﻿var infoArr = [];
var dataInfo = [];
var inputDataInfo = [];
// var myComp = app.project.activeItem;
// 去掉左右空格
function trim(s) {
    return s.replace(/(^\s*)|(\s*$)/g, "");
}

function getInfo(layer, fps, currentProp, currentObj) {
    if (currentProp.numProperties && currentProp.numProperties > 0) {
        for (var i = 1; i <= currentProp.numProperties; i++) {
            var currProp = currentProp.property(i);
            var obj = {};
            obj.name = currProp.name;
            // obj.matchName = trim(currProp.matchName.replace("ADBE", ""));
            obj.matchName = trim(currProp.matchName);
            obj.proptype = [];
            currentObj.proptype.push(obj);
            getInfo(layer, fps, currProp, obj);
        }
    } else {
        delete currentObj.proptype;

        var indexArr = [currentProp.propertyIndex];
        while(currentProp.parentProperty && currentProp.parentProperty.propertyIndex){
            indexArr.unshift(currentProp.parentProperty.propertyIndex);
            currentProp = currentProp.parentProperty
        }
        var propCurrName = null;
        for (var i = 0; i < indexArr.length; i++){
            if(propCurrName === null){
                propCurrName = layer.property(indexArr[i])
            }else{
                propCurrName = propCurrName.property(indexArr[i])
            }
        }

        // var propCurrName = layer.property(trim(currentProp.matchName.replace("ADBE", "")));
        var numKeys = 0;
        if (propCurrName) {
            numKeys = propCurrName.numKeys;
        }
        // 关键帧的数量要大于等于2
        if (numKeys >= 2) {
            keyTimeFirst = propCurrName.keyTime(1);
            keyTimeLast = propCurrName.keyTime(numKeys);
            currentObj.firstFrame = Math.floor(keyTimeFirst * fps);
            currentObj.lastFrame = Math.ceil(keyTimeLast * fps);
            frameCount = currentObj.lastFrame - currentObj.firstFrame;
            currentObj.data = [];
            for (var l = 0; l <= frameCount; l++) {
                currentObj.data.push(propCurrName.valueAtTime(l / fps, false).toString().split(','));
            }
        }
    }
}

function getAllInfo() {
    var infoArr = [];
    // 获取所有合成（项目）
    for (var n = 1; n <= app.project.numItems; n++) {
        var compInfo = {};
        var myComp = app.project.item(n);
        var fps = myComp.frameRate;
        //$.writeln(myComp.frameDuration);
        //$.writeln(myComp.frameRate);
        compInfo.name = myComp.name;
        compInfo.matchName = myComp.name;
        compInfo.typeName = myComp.typeName;
        compInfo.fps = fps;
        compInfo.width = myComp.width;
        compInfo.height = myComp.height;
        compInfo.layers = [];
        if (fps !== undefined) {
            // 获取合成（项目）下的所有层
            for (var i = 1; i <= myComp.numLayers; i++) {
                var currLayer = myComp.layer(i);
                var layerInfo = {};
                layerInfo.name = currLayer.name;
                layerInfo.matchName = currLayer.name;
                layerInfo.proptype = [];
                getInfo(currLayer, fps, currLayer, layerInfo);
                compInfo.layers.push(layerInfo);
            }
            infoArr.push(compInfo);
        }
    }
    return infoArr;
}

function getDataInfo(currData, path) {
    if (currData.proptype && currData.proptype.length > 0) {
        for (var i = 0; i < currData.proptype.length; i++) {
            if (currData.proptype[i].proptype && currData.proptype[i].proptype.length > 0) {
                // 千万不能给变量重新赋值，要不然值变化后，for循环里面在引用的时候，就不一样了，直接当做参数传过去
                // currData = currData.proptype[i]
                // path += "/" + currData.proptype[i].matchName + "/" + i + "/";
                getDataInfo(currData.proptype[i], path + "/" + currData.proptype[i].matchName + "/" + i + "/");
            } else {
                if (currData.proptype[i].data && currData.proptype[i].data.length > 0) {
                    dataInfo.push(path + currData.proptype[i].matchName + "/" + i + "/data");
                }
            }
        }
    } else {
        if (currData.data && currData.data.length > 0) {
            dataInfo.push(path + currData.matchName + i + "/data");
        }
    }
}

function getDataObjInfo() {
    for (var i = 0; i < infoArr.length; i++) {
        if (infoArr[i].layers && infoArr[i].layers.length > 0) {
            for (var j = 0; j < infoArr[i].layers.length; j++) {
                getDataInfo(infoArr[i].layers[j], infoArr[i].matchName + "/" + i + "/" + infoArr[i].layers[j].matchName + "/" + j);
            }
        }
    }
}



function getInputInfo() {
    var pullDownArr = [];
    var positionArr = [];
    for (var i = 0; i < dataInfo.length; i++) {
        var currInfo = dataInfo[i].split("/");
        var pullDownStr = "";
        var positionStr = "";
        for (var j = 0; j < (currInfo.length - 1) / 2; j++) {
            pullDownStr += currInfo[j * 2] + "/";
            positionStr += currInfo[j * 2 + 1] + "/";
        }
        pullDownArr.push(pullDownStr.substr(0, pullDownStr.length - 1));
        positionArr.push(positionStr.substr(0, positionStr.length - 1));
    }
    return [infoArr, pullDownArr, positionArr];
}
//$.writeln(dataInfo);
//$.writeln(infoArr[0].layers[0].proptype[4].proptype[1].data);
function run() {
    var scriptName = "Copy Curve";
    var newLocation = Folder.selectDialog("Select a output folder...");
    if (newLocation != null) {
        app.beginUndoGroup(scriptName);
        infoArr = getAllInfo();
        getDataObjInfo();
        inputDataInfo = getInputInfo();
        var chartSys = "\\";
        if (File.fs == "Macintosh") {
            chartSys = "/";
        }
        // var dataFile = new File(File($.fileName).path + chartSys + "data.json");
        var dataFile = new File(newLocation.toString() + chartSys + "data.json");
        dataFile.open("w");
        dataFile.encoding = "UTF-8";
        dataFile.write(JSON.stringify(inputDataInfo));
        dataFile.close();
        alert("写入完成！！！");
        app.endUndoGroup();
    }
}

run();

/*
$.writeln(File.fullName);
$.writeln(File.fs);
$.writeln($.fileName);
$.writeln(File($.fileName).path)
*/