AEProp = tn.knob("AEProptypes").value()
AEFPS = float(tn.knob("AEFPS").value())
AEScreenWidth = float(tn.knob("AEScreenWidth").value())
AEScreenHeight = float(tn.knob("AEScreenHeight").value())
AESingleProps = tn.knob("AEProptypesSingle").values()
AESingleProp = tn.knob("AEProptypesSingle").value()
AEFirstFrame = int(tn.knob("AEFirstFrame").value())
AELastFrame = int(tn.knob("AELastFrame").value())
AEFrameCount = AELastFrame - AEFirstFrame

AEIndexArr = [i for i, name in enumerate(AESingleProps) if name == AESingleProp]
AEIndex = 0
if len(AESingleProps) > 1:
    AEIndex = AEIndexArr[0] - 1

positionArr = getInfoData(AEProp)
AEPropPath = fileContent[positionArr[0]]["layers"][positionArr[1]]["path"].encode("utf-8")
nameArr = AEPropPath.split("/")
name = nameArr[len(nameArr) - 1].lower()
flag = ""
if "scale" in name:
    flag = "scale"
elif "position" in name:
    flag = "position"
elif "opacity" in name:
    flag = "opacity"
else:
    flag = ""



tNodeName = tn.knob("NukeTargetNodes").value()
tAttrMultipleName = tn.knob("NukeTargetAttributeMultiple").value()
tAttrSingleName = tn.knob("NukeTargetAttributeSingle").value()
tAttrSingleNames = tn.knob("NukeTargetAttributeSingle").values()
NukeFPS = float(tn.knob("NukeFPS").value())
NukeScreenWidth = float(tn.knob("NukeScreenWidth").value())
NukeScreenHeight = float(tn.knob("NukeScreenHeight").value())
NukeFirstFrame = int(tn.knob("NukeFirstFrame").value())
NukeLastFrame = int(tn.knob("NukeLastFrame").value())
NukeFrameCount = NukeLastFrame - NukeFirstFrame
NukeStartingPosition = float(tn.knob("NukeStartingPosition").value())

ratio = AEFrameCount * 1.0 / NukeFrameCount

tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
tIndex = 0
if len(tAttrSingleNames) > 1:
    tIndex = tIndexArr[0] - 1

tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)
tAnimation.clearAnimated(tIndex)
tAnimation.setAnimated(tIndex)
tAnimationCurves = tAnimation.animations()
tAnimationCurve = None
for j in range(len(tAnimationCurves)):
    if tAnimationCurves[j].knobIndex() == tIndex:
        tAnimationCurve = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()[j]



if flag == "scale" or flag == "opacity":
    for i in range(AEFrameCount + 1):
        tAnimation.setValueAt(float(dataInfo[i][AEIndex]) / 100 + NukeStartingPosition, i / ratio + NukeFirstFrame, tIndex)
elif flag == "position":
    if AEIndex == 0:
        for i in range(AEFrameCount + 1):
            tAnimation.setValueAt(float(dataInfo[i][AEIndex]) - float(dataInfo[0][AEIndex]) / (AEScreenWidth/ NukeScreenWidth) + NukeStartingPosition, i / ratio + NukeFirstFrame, tIndex)
    elif AEIndex == 1:
        for i in range(AEFrameCount + 1):
            tAnimation.setValueAt(-(float(dataInfo[i][AEIndex]) - float(dataInfo[0][AEIndex])) / (AEScreenHeight/ NukeScreenHeight) + NukeStartingPosition, i / ratio + NukeFirstFrame, tIndex)
    else:
        for i in range(AEFrameCount + 1):
            tAnimation.setValueAt(float(dataInfo[i][AEIndex]) - float(dataInfo[0][AEIndex]) + NukeStartingPosition, i / ratio + NukeFirstFrame, tIndex)
else:
    for i in range(AEFrameCount + 1):
        tAnimation.setValueAt(float(dataInfo[i][AEIndex]) + NukeStartingPosition, i / ratio + NukeFirstFrame, tIndex)