﻿var infoArr = [];

/**
 * 去掉左右空格
 *
 * @param {*} s     传入的字符串
 * @returns         返回值
 */
function trim(s) {
    return s.replace(/(^\s*)|(\s*$)/g, "");
}

/**
 * 判断是否为数字
 *
 * @param {*} val   传入的数据
 * @returns         返回值
 */
function isNumber(val){
    //非负浮点数
    var regPos = /^\d+(\.\d+)?$/;
    //负浮点数
    var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/;
    if(regPos.test(val) || regNeg.test(val)){
        return true;
    }else{
        return false;
    }
}

/**
 * 递归获取所有属性
 *
 * @param {*} layer             AE里面的层
 * @param {*} fps               当前合成项目的帧频
 * @param {*} currentProp       当前属性对象
 * @param {*} currentObj        当前层的layers对象，用来存放符合条件的属性数据
 */
function getInfo(layer, fps, currentProp, currentObj) {
    // 如果当前属性还有子级属性存在，则继续查找
    if (currentProp.numProperties && currentProp.numProperties > 0) {
        for (var i = 1; i <= currentProp.numProperties; i++) {
            var currProp = currentProp.property(i);
            getInfo(layer, fps, currProp, currentObj);
        }
    } else {
        // 获取到当前属性到层的名称
        var indexArrMatchName = [currentProp.matchName];
        var indexArrName = [currentProp.name];
        var indexArr = [currentProp.matchName];
        while(currentProp.parentProperty && currentProp.parentProperty.matchName){
            indexArrMatchName.unshift(currentProp.parentProperty.matchName);
            indexArrName.unshift(currentProp.parentProperty.name);
            indexArr.unshift(currentProp.parentProperty.matchName);
            currentProp = currentProp.parentProperty
        }
        indexArr[0] = indexArrName[0];
        var propCurrName = null;
        for (var i = 0; i < indexArrMatchName.length; i++){
            if(propCurrName === null){
                propCurrName = layer.property(indexArrMatchName[i])
            }else{
                propCurrName = propCurrName.property(indexArrMatchName[i])
            }
        }
        // 获取当前属性关键帧的数量
        var numKeys = 0;
        if (propCurrName) {
            numKeys = propCurrName.numKeys;
        }
        // 关键帧的数量要大于等于2
        if (numKeys >= 2) {
            var isNumberFlag = true;
            var dataObj = {
                "path": "",
                "pathName": "",
                "pathMatchName": "",
                "firstFrame": 0,
                "lastFrame": 0,
                "data": []
            };
            keyTimeFirst = propCurrName.keyTime(1);
            keyTimeLast = propCurrName.keyTime(numKeys);
            dataObj.path = indexArr.join("/");
            dataObj.pathName = indexArrName.join("/");
            dataObj.pathMatchName = indexArrMatchName.join("/");
            dataObj.firstFrame = Math.floor(keyTimeFirst * fps);
            dataObj.lastFrame = Math.ceil(keyTimeLast * fps);
            frameCount = dataObj.lastFrame - dataObj.firstFrame;
            for (var l = 0; l <= frameCount; l++) {
                valueArr = propCurrName.valueAtTime(l / fps, false).toString().split(',');
                for (var j = 0; j < valueArr.length; j++){
                    if(!isNumber(valueArr[j])){
                        isNumberFlag = false;
                        break;
                    }else{
                        valueArr[j] = parseFloat(valueArr[j]);
                    }
                }
                dataObj.data.push(valueArr);
            }
            if (isNumberFlag){
                currentObj.push(dataObj);
            }
        }
    }
}


/**
 * 获取所有属性的信息
 *
 * @returns
 */
function getAllInfo() {
    var infoArr = [];
    // 获取所有合成（项目）
    for (var n = 1; n <= app.project.numItems; n++) {
        var compInfo = {};
        var myComp = app.project.item(n);
        var fps = myComp.frameRate;
        compInfo.name = myComp.name;
        compInfo.matchName = myComp.name;
        compInfo.typeName = myComp.typeName;
        compInfo.fps = fps;
        compInfo.width = myComp.width;
        compInfo.height = myComp.height;
        compInfo.layers = [];
        if (fps !== undefined) {
            // 获取合成（项目）下的所有层
            for (var i = 1; i <= myComp.numLayers; i++) {
                var currLayer = myComp.layer(i);
                getInfo(currLayer, fps, currLayer, compInfo.layers);
                // compInfo.layers.push(layerInfo);
            }
            if(compInfo.layers.length > 0){
                infoArr.push(compInfo);
            }
        }
    }
    return infoArr;
}


/**
 * 入口(运行)函数， 选择输出文件目录，导出数据文件
 *
 */
function run() {
    var scriptName = "Copy Curve";
    var newLocation = Folder.selectDialog("Select a output folder...");
    if (newLocation != null) {
        app.beginUndoGroup(scriptName);
        infoArr = getAllInfo();
        var chartSys = "\\";
        if (File.fs == "Macintosh") {
            chartSys = "/";
        }
        var dataFile = new File(newLocation.toString() + chartSys + "data.json");
        dataFile.open("w");
        dataFile.encoding = "UTF-8";
        dataFile.write(JSON.stringify(infoArr));
        dataFile.close();
        alert("写入完成！！！");
        app.endUndoGroup();
    }
}

run();