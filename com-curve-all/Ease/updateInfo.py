
tn = nuke.thisNode()

easeTargetNodesName = tn.knob("EaseTargetNodes").value()
easeMultipleAttrName = tn.knob("EaseAttributeMultiple").value()
easeAttributeSingleName = tn.knob("EaseAttributeSingle").value()

def setEaseTargetInfo():
    easeTargetNodeName = tn.knob("EaseTargetNodes").value()
    easeAttributeMultipleName = tn.knob("EaseAttributeMultiple").value()
    easeAttributeSingleName = tn.knob("EaseAttributeSingle").value()
    easeAttributeSingleNames = tn.knob("EaseAttributeSingle").values()

    easeFirstFrame = tn.knob("EaseFirstFrame").value()
    easeLastFrame = tn.knob("EaseLastFrame").value()
    easeFirstFrameValue = tn.knob("EaseFirstFrameValue").value()
    easeLastFrameValue = tn.knob("EaseLastFrameValue").value()

    if easeTargetNodeName != "None" and easeAttributeMultipleName != "None" and easeAttributeSingleName != "None":
        if easeLastFrame > easeFirstFrame and easeFirstFrameValue != easeLastFrameValue:
            tn.knob("EaseState").setValue("ok")
        else:
            tn.knob("EaseState").setValue("信息填写不完整！！！")
    elif easeTargetNodeName != "None" and easeAttributeMultipleName != "None" and len(easeAttributeSingleNames) <= 1:
        if easeLastFrame > easeFirstFrame and easeFirstFrameValue != easeLastFrameValue:
            tn.knob("EaseState").setValue("ok")
        else:
            tn.knob("EaseState").setValue("信息填写不完整！！！")
    else:
        tn.knob("EaseState").setValue("信息填写不完整！！！")

def hideEaseTargetInfo():
    tn.knob("EaseFirstFrame").setValue(0)
    tn.knob("EaseFirstFrameValue").setValue(0)
    tn.knob("EaseLastFrame").setValue(0)
    tn.knob("EaseLastFrameValue").setValue(0)

def getEaseTargetInfo():
    tNodeName = tn.knob("EaseTargetNodes").value()
    tAttrMultipleName = tn.knob("EaseAttributeMultiple").value()
    tAttrSingleName = tn.knob("EaseAttributeSingle").value()
    tAttrSingleNames = tn.knob("EaseAttributeSingle").values()

    if (tNodeName != "None" and tAttrMultipleName != "None" and tAttrSingleName != "None") or (tNodeName != "None" and tAttrMultipleName != "None" and len(tAttrSingleNames) <= 1):
        tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
        tIndex = 0
        if len(tAttrSingleNames) > 1:
            tIndex = tIndexArr[0] - 1
        tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

        if tAnimation.isAnimated() and (tAnimation.animation(tIndex) != None) and len(tAnimation.animation(tIndex).keys()) > 1:
            tAnimationCurve = tAnimation.animation(tIndex)
            tkeysCount = len(tAnimationCurve.keys())
            tn.knob("EaseFirstFrame").setValue(tAnimationCurve.keys()[0].x)
            tn.knob("EaseFirstFrameValue").setValue(tAnimationCurve.keys()[0].y)
            tn.knob("EaseLastFrame").setValue(tAnimationCurve.keys()[tkeysCount - 1].x)
            tn.knob("EaseLastFrameValue").setValue(tAnimationCurve.keys()[tkeysCount - 1].y)
        elif tAnimation.hasExpression() and tAnimation.hasExpression(tIndex):

            express = tAnimation.animation(tIndex).expression()

            count = 0
            index = 0
            for i in express:
                if count == 2:
                    break
                else:
                    if i == ":":
                        count += 1
                index += 1

            strArr = express[0:index - 2].replace("frame", "").replace("<", "").replace(">", "").replace(" ", "").split(
                ":")
            frameInfoArr = []
            for str in strArr:
                currStrArr = str.split("?")
                frameInfoArr.append(int(float(currStrArr[0])))
                frameInfoArr.append(float(currStrArr[1]))

            tn.knob("EaseFirstFrame").setValue(frameInfoArr[0])
            tn.knob("EaseFirstFrameValue").setValue(frameInfoArr[1])
            tn.knob("EaseLastFrame").setValue(frameInfoArr[2])
            tn.knob("EaseLastFrameValue").setValue(frameInfoArr[3])
        else:
            hideEaseTargetInfo()
    else:
        hideEaseTargetInfo()

    setEaseTargetInfo()


def updateEaseTargetNodes():
    tn.knob("EaseTargetNodes").setValue(easeTargetNodesName)

    easeAttributeMultiple = []
    easeAttributeSingle = []

    if easeTargetNodesName != "None":
        for knob in nuke.toNode(easeTargetNodesName).allKnobs():
            knobName = knob.name()
            if knobName != "":
                easeAttributeMultiple.append(knobName)
        easeAttributeMultiple.sort()
        easeAttributeMultiple.insert(0, "None")
        tn.knob("EaseAttributeMultiple").setValues(easeAttributeMultiple)
    else:
        tn.knob("EaseAttributeMultiple").setValues(["None"])
    tn.knob("EaseAttributeMultiple").setValue(easeMultipleAttrName)

    if easeMultipleAttrName != "None":
        multipleAttrKnobs = nuke.toNode(easeTargetNodesName).knob(easeMultipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.vect())):
                    easeAttributeSingle.append(multipleAttrKnobs.names(i))
                easeAttributeSingle.sort()
                easeAttributeSingle.insert(0, "None")
                tn.knob("EaseAttributeSingle").setValues(easeAttributeSingle)
            else:
                tn.knob("EaseAttributeSingle").setValues(["None"])
        else:
            tn.knob("EaseAttributeSingle").setValues(["None"])
    else:
        tn.knob("EaseAttributeSingle").setValues(["None"])
    tn.knob("EaseAttributeSingle").setValue(easeAttributeSingleName)

    getEaseTargetInfo()


updateEaseTargetNodes()
