
tn = nuke.thisNode()
nodeName = tn.knob("sourceNodes").value()
multipleAttrName = tn.knob("attributeMultiple").value()
attributeSingleName = tn.knob("attributeSingle").value()
targetNodesName = tn.knob("targetNodes").value()
targetMultipleAttrName = tn.knob("targetAttributeMultiple").value()
targetAttributeSingleName = tn.knob("targetAttributeSingle").value()

def updateSourceNodes():
    allNodes = nuke.root().nodes()
    oldSourceNodes = tn.knob("sourceNodes").values()
    sourceNodes = ["None"]

    for node in allNodes:
        sourceNodes.append(node.name())

    ots = list(set(oldSourceNodes).difference(set(sourceNodes)))
    sto = list(set(sourceNodes).difference(set(oldSourceNodes)))

    if len(ots) > 0 or len(sto) > 0:
        tn.knob("sourceNodes").setValues(sourceNodes)
        tn.knob("targetNodes").setValues(sourceNodes)


    tn.knob("sourceNodes").setValue(nodeName)

    attributeMultiple = ["None"]
    attributeSingle = ["None"]

    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "" and nuke.toNode(nodeName)[knobName].isAnimated():
                attributeMultiple.append(knobName)

        tn.knob("attributeMultiple").setValues(attributeMultiple)
    else:
        tn.knob("attributeMultiple").setValues(["None"])
    tn.knob("attributeMultiple").setValue(multipleAttrName)

    if multipleAttrName != "None":
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.animations())):
                    attributeSingle.append(multipleAttrKnobs.animations()[i].knobAndFieldName().split(".")[1])
                tn.knob("attributeSingle").setValues(attributeSingle)
            else:
                tn.knob("attributeSingle").setValues(["None"])
        else:
            tn.knob("attributeSingle").setValues(["None"])
    else:
        tn.knob("attributeSingle").setValues(["None"])
    tn.knob("attributeSingle").setValue(attributeSingleName)
    #sourceTargetNodes.getSourceInfo
    getSourceInfo()


updateSourceNodes()





def updateTargetNodes():
    tn.knob("targetNodes").setValue(targetNodesName)

    targetAttributeMultiple = []
    targetAttributeSingle = []

    if targetNodesName != "None":
        for knob in nuke.toNode(targetNodesName).allKnobs():
            knobName = knob.name()
            if knobName != "":
                targetAttributeMultiple.append(knobName)
        targetAttributeMultiple.sort()
        targetAttributeMultiple.insert(0, "None")
        tn.knob("targetAttributeMultiple").setValues(targetAttributeMultiple)
    else:
        tn.knob("targetAttributeMultiple").setValues(["None"])
    tn.knob("targetAttributeMultiple").setValue(targetMultipleAttrName)

    if targetMultipleAttrName != "None":
        multipleAttrKnobs = nuke.toNode(targetNodesName).knob(targetMultipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.vect())):
                    targetAttributeSingle.append(multipleAttrKnobs.names(i))
                targetAttributeSingle.sort()
                targetAttributeSingle.insert(0, "None")
                tn.knob("targetAttributeSingle").setValues(targetAttributeSingle)
            else:
                tn.knob("targetAttributeSingle").setValues(["None"])
        else:
            tn.knob("targetAttributeSingle").setValues(["None"])
    else:
        tn.knob("targetAttributeSingle").setValues(["None"])
    tn.knob("targetAttributeSingle").setValue(targetAttributeSingleName)
    #sourceTargetNodes.getTargetInfo
    getTargetInfo()


updateTargetNodes()
