import os
# For incompletely cracked NUKE, you need to set NUKE_INTERACTIVE to a non-zero value (must be a string)
os.environ["NUKE_INTERACTIVE"] = "1"

import json

# NUKE can't introduce nuke package without cracking, and it can't connect to service when it runs.
import nuke

def exportVideo():
    nuke.root()["last_frame"].setValue(10)
    nuke.root()["format"].setValue("1K_Super_35(full-ap)")

    readNode = nuke.nodes.Read(name="Read2", file="/Server/Houdini/HTN/render/box.mantra_render_HD.####.exr")
    readNode.knob("first").setValue(1)
    readNode.knob("last").setValue(10)
    readNode.knob("origfirst").setValue(1)
    readNode.knob("origlast").setValue(10)

    writeNode = nuke.nodes.Write(name="Write1", file="/Server/Nuke/box.mov", file_type="mov")
    writeNode.setInput(0, readNode)
    nuke.execute(writeNode, 1, nuke.root().lastFrame(), 1)
    print "Success!!!"

exportVideo()