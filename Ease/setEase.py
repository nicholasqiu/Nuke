'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-13 14:57:18
@LastEditTime: 2019-08-20 16:40:39
@LastEditors: Please set LastEditors
'''

def setCurveExpression(startFrame, endFrame, startValue, endValue, tweenType, easeType):
    baseline_value = (endValue - startValue) / 2
    mix_value = 1
    easePrefix = "frame < %s ? %s : frame > %s ? %s : " % (startFrame, startValue, endFrame, endValue)
    c = "(%s - %s)" % (endValue, startValue)
    b = "%s" % (startValue)
    t = "(frame - %s)" % (startFrame)
    d = "(%s - %s)" % (endFrame, startFrame)
    td = "((frame - %s) / (%s - %s))" % (startFrame, endFrame, startFrame)
    td2 = "((frame - %s) / ((%s - %s) / 2))" % (startFrame, endFrame, startFrame)
    dtd = "((%s - %s) - (frame - %s)) / (%s - %s)" % (endFrame, startFrame, startFrame, endFrame, startFrame)
    t2 = "((%s - %s) - (frame - %s) * 2) / (%s - %s)" % (endFrame, startFrame, startFrame, endFrame, startFrame)
    t2d = "((frame - %s) * 2 - (%s - %s)) / (%s - %s)" % (startFrame, endFrame, startFrame, endFrame, startFrame)

    Tweens = {
        "Linear": {
            "easeIn": "%s * %s / %s + %s" %(c,t,d,b),
            "easeOut": "%s * %s / %s + %s" %(c,t,d,b),
            "easeInOut": "%s * %s / %s + %s" %(c,t,d,b)
        },
        "Quadratic": {
            "easeIn": "%s * %s * %s + %s" %(c,td,td,b),
            "easeOut": "-%s * %s * (%s-2) + %s" %(c,td,td,b),
            "easeInOut": "(%s < 1 ? %s/2 * %s * %s + %s : -%s/2 * ((%s-1)*((%s-1)-2) - 1) + %s)" %(td2,c,td2,td2,b,c,td2,td2,b)
        },
        "Cubic": {
            "easeIn": "%s * %s * %s * %s + %s" %(c,td,td,td,b),
            "easeOut": "%s * ((%s - 1) * (%s - 1) * (%s - 1) + 1) + %s" %(c,td,td,td,b),
            "easeInOut": "%s < 1 ? %s / 2 * %s * %s * %s + %s : %s / 2  * ((%s - 2) * (%s - 2) * (%s - 2) + 2) + %s" %(td2,c,td2,td2,td2,b,c,td2,td2,td2,b)
        },
        "Quartic": {
            "easeIn": "%s * %s * %s * %s * %s + %s" %(c,td,td,td,td,b),
            "easeOut": "-%s * ((%s - 1) * (%s - 1) * (%s - 1) * (%s - 1) - 1) + %s" %(c,td,td,td,td,b),
            "easeInOut": "%s < 1 ? %s / 2 * %s * %s * %s * %s + %s : -%s / 2  * ((%s - 2) * (%s - 2) * (%s - 2) * (%s - 2) - 2) + %s" %(td2,c,td2,td2,td2,td2,b,c,td2,td2,td2,td2,b)
        },
        "Quintic": {
            "easeIn": "%s * %s * %s * %s * %s * %s + %s" %(c,td,td,td,td,td,b),
            "easeOut": "%s * ((%s - 1) * (%s - 1) * (%s - 1) * (%s - 1) * (%s - 1) + 1) + %s" %(c,td,td,td,td,td,b),
            "easeInOut": "%s < 1 ? %s / 2 * %s * %s * %s * %s * %s + %s : %s / 2  * ((%s - 2) * (%s - 2) * (%s - 2) * (%s - 2) * (%s - 2) + 2) + %s" %(td2,c,td2,td2,td2,td2,td2,b,c,td2,td2,td2,td2,td2,b)
        },
        "Sinusoidal": {
            "easeIn": "-%s * cos(%s * (3.1415926 / 2)) + %s + %s" %(c,td,c,b),
            "easeOut": "%s * sin(%s * (3.1415926 / 2)) + %s" %(c,td,b),
            "easeInOut": "-%s / 2 * (cos(3.1415926 * %s) - 1) + %s" %(c,td,b)
        },
        "Exponential": {
            "easeIn": "%s == 0 ? %s : %s * 2**(10 * (%s - 1)) + %s" %(t,b,c,td,b),
            "easeOut": "%s == %s ? %s + %s : %s * (-(2**(-10 * %s)) + 1) + %s" %(t,d,b,c,c,td,b),
            "easeInOut": "%s == 0 ? %s : (%s == %s ? %s + %s : (%s < 1 ? %s / 2 * (2 ** (10 * (%s - 1)) ) + %s : %s / 2 * (-(2 ** (-10 * (%s-1))) + 2) + %s))" %(t,b,t,d,b,c,td2,c,td2,b,c,td2,b)
        },
        "Circular": {
            "easeIn": "-%s * (sqrt(1 - %s * %s) - 1) + %s" %(c,td,td,b),
            "easeOut": "%s * sqrt(1 - (%s - 1) * (%s - 1)) + %s" %(c,td,td,b),
            "easeInOut": "%s < 1 ? -%s / 2 * (sqrt(1 - %s * %s) - 1) + %s : %s / 2 * (sqrt(1 - (%s - 2) * (%s - 2)) + 1) + %s" %(td2,c,td2,td2,b,c,td2,td2,b)
        },
        "Elastic": {
            "easeIn": "-(%s * (2**(10 * (%s - 1))) * sin( ((%s - 1) * %s-((%s * 0.3) / 4)) * (3.14159 * 2) / (%s * 0.3) )) + %s" %(c,td,td,d,d,d,b),
            "easeOut": "%s * (2**(-10 * %s)) * sin( (%s * %s-((%s * 0.3) / 4)) * (3.14159 * 2) / (%s * 0.3) ) + %s + %s" %(c,td,td,d,d,d,c,b),
            "easeInOut": "%s < 1 ? -0.5 * (%s * (2**(10 * (%s - 1))) * sin( ((%s - 1) * %s-((%s * 0.3 * 1.5) / 4)) * (3.14159 * 2) / (%s * 0.3 * 1.5) )) + %s : %s * (2**(-10 * (%s - 1))) * sin( ((%s - 1) * %s-((%s * 0.3 * 1.5) / 4)) * (3.14159 * 2) / (%s * 0.3 * 1.5) ) * 0.5 + %s + %s" %(td2,c,td2,td2,d,d,d,b,c,td2,td2,d,d,d,c,b)
        },
        "Back": {
            "easeIn": "%s * %s * %s * ((%s + 1) * %s - %s) + %s" %(c,td,td,1.70158,td,1.70158,b),
            "easeOut": "%s * ((%s/%s-1)*(%s/%s-1)*((%s+1)*(%s/%s-1) + %s) + 1) + %s" %(c,t,d,t,d,1.70158,t,d,1.70158,b),
            "easeInOut": "%s < 1 ? %s / 2 * (%s * %s * (((%s * 1.525 + 1) * %s) - %s * 1.525)) + %s : %s / 2 * ((%s - 2) * (%s - 2) * (((%s * 1.525 + 1) * (%s - 2)) + %s * 1.525) + 2) + %s" %(td2,c,td2,td2,1.70158,td2,1.70158,b,c,td2,td2,1.70158,td2,1.70158,b)
        },
        "Bounce": {
            "easeIn": "%s < (1/2.75) ? %s - %s * (7.5625 * %s * %s) + %s : %s < (2/2.75) ? %s -  %s * (7.5625 * (%s - (1.5/2.75)) * (%s - (1.5/2.75)) + 0.75) + %s : %s < (2.5/2.75) ? %s -  %s * (7.5625 * (%s - (2.25/2.75)) * (%s - (2.25/2.75)) + 0.9375) + %s : %s -  %s * (7.5625 * (%s - (2.625/2.75)) * (%s - (2.625/2.75)) + 0.984375) + %s" %(dtd,c,c,dtd,dtd,b,dtd,c,c,dtd,dtd,b,dtd,c,c,dtd,dtd,b,c,c,dtd,dtd,b),
            "easeOut": "%s < (1/2.75) ? %s * (7.5625 * %s * %s) + %s : %s < (2/2.75) ? %s * (7.5625 * (%s - (1.5/2.75)) * (%s - (1.5/2.75)) + 0.75) + %s : %s < (2.5/2.75) ? %s * (7.5625 * (%s - (2.25/2.75)) * (%s - (2.25/2.75)) + 0.9375) + %s : %s * (7.5625 * (%s - (2.625/2.75)) * (%s - (2.625/2.75)) + 0.984375) + %s" %(td,c,td,td,b,td,c,td,td,b,td,c,td,td,b,c,td,td,b),
            "easeInOut": "%s < %s / 2 ? (%s < (1/2.75) ? (%s - %s * (7.5625 * %s * %s)) * 0.5 + %s : %s < (2/2.75) ? (%s -  %s * (7.5625 * (%s - (1.5/2.75)) * (%s - (1.5/2.75)) + 0.75)) * 0.5 + %s : %s < (2.5/2.75) ? (%s -  %s * (7.5625 * (%s - (2.25/2.75)) * (%s - (2.25/2.75)) + 0.9375)) * 0.5 + %s : (%s -  %s * (7.5625 * (%s - (2.625/2.75)) * (%s - (2.625/2.75)) + 0.984375)) * 0.5 + %s) : (%s < (1/2.75) ? (%s * (7.5625 * %s * %s)) * 0.5 + %s * 0.5 + %s : %s < (2/2.75) ? (%s * (7.5625 * (%s - (1.5/2.75)) * (%s - (1.5/2.75)) + 0.75)) * 0.5 + %s * 0.5 + %s : %s < (2.5/2.75) ? (%s * (7.5625 * (%s - (2.25/2.75)) * (%s - (2.25/2.75)) + 0.9375)) * 0.5 + %s * 0.5 + %s : (%s * (7.5625 * (%s - (2.625/2.75)) * (%s - (2.625/2.75)) + 0.984375)) * 0.5 + %s * 0.5 + %s)" % (t, d, t2, c, c, t2, t2, b, t2, c, c, t2, t2, b, t2, c, c, t2, t2, b, c, c, t2, t2, b, t2d, c, t2d, t2d, c, b, t2d, c, t2d, t2d, c, b, t2d, c, t2d, t2d, c, b, c, t2d, t2d, c, b)
        }
    }

    return easePrefix + Tweens[tweenType][easeType]


def expressionToKeys(tAnimation, tIndex, tAnimationCurve, tFirstFrame, tLastFrame, tLastFrameValue):
    stepAccuracy = 4.0
    if stepAccuracy <= 0:
        stepAccuracy = 1.0

    valueArr = []
    for i in range(int((tLastFrame - tFirstFrame) * stepAccuracy)):
        valueArr.append(tAnimationCurve.evaluate(i / stepAccuracy + tFirstFrame))
    valueArr.append(tAnimationCurve.evaluate(tLastFrame))

    # tAnimation.clearAnimated(tIndex)
    # print tAnimation.animations()[tIndex]
    # tAnimation.deleteAnimation(tAnimation.animations()[tIndex])

    if not tAnimationCurve.noExpression():
        tAnimation.deleteAnimation(tAnimationCurve)
    tAnimation.setAnimated(tIndex)

    # if tAnimation.isAnimated():
    #     tAnimationCurve.clear()

    for i in range(int((tLastFrame - tFirstFrame) * stepAccuracy)):
        tAnimation.setValueAt(valueArr[i], i / stepAccuracy + tFirstFrame, tIndex)
    tAnimation.setValueAt(tLastFrameValue, tLastFrame, tIndex)


def getExpressValue(animationCurve):
    express = animationCurve.expression()
    count = 0
    index = 0
    for i in express:
        if count == 2:
            break
        else:
            if i == ":":
                count += 1
        index += 1

    strArr = express[0:index - 2].replace("frame", "").replace("<", "").replace(">", "").replace(" ", "").split(":")
    frameInfoArr = []
    for str in strArr:
        currStrArr = str.split("?")
        frameInfoArr.append(int(float(currStrArr[0])))
        frameInfoArr.append(float(currStrArr[1]))

    return [frameInfoArr[0], frameInfoArr[1], frameInfoArr[2], frameInfoArr[3]]


if tn.knob("EaseState").value() == "ok":
    tweenType = tn.knob("EaseTweenType").value()
    easeType = tn.knob("EaseEaseType").value()
    tNodeName = tn.knob("EaseTargetNodes").value()
    tAttrMultipleName = tn.knob("EaseAttributeMultiple").value()
    tAttrSingleName = tn.knob("EaseAttributeSingle").value()
    tAttrSingleNames = tn.knob("EaseAttributeSingle").values()
    tFirstFrame = tn.knob("EaseFirstFrame").value()
    tLastFrame = tn.knob("EaseLastFrame").value()
    tFirstFrameValue = tn.knob("EaseFirstFrameValue").value()
    tLastFrameValue = tn.knob("EaseLastFrameValue").value()


    isSingle = True
    if tAttrSingleName.lower() == "all":
        isSingle = False

    if isSingle:
        tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
        tIndex = 0
        if len(tAttrSingleNames) > 1:
            tIndex = tIndexArr[0] - 1
        tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

        tAnimation.setAnimated(tIndex)

        tAnimationCurves = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()
        tAnimationCurve = None
        for j in range(len(tAnimationCurves)):
            if tAnimationCurves[j].knobIndex() == tIndex:
                tAnimationCurve = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()[j]

        if tAnimation.isAnimated():
            tAnimationCurve.clear()

        tAnimationCurve.setExpression(setCurveExpression(tFirstFrame, tLastFrame, tFirstFrameValue, tLastFrameValue, tweenType, easeType))

    else:
        tIndex = 0
        tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

        num = 0
        if len(tAttrSingleNames) > 1:
            num = len(tAttrSingleNames) - 1 - 1
        for n in range(num):
            tIndex = n
    
            firstFrame = 0
            lastFrame = 0
            firstFrameValue = 0
            lastFrameValue = 0

            
            tAnimationCurves = tAnimation.animations()

            tAnimationCurve = None
            for j in range(len(tAnimationCurves)):
                if tAnimationCurves[j].knobIndex() == tIndex:
                    tAnimationCurve = tAnimationCurves[j]

            
            if tAnimationCurve == None:
                continue


            if not tAnimationCurve.noExpression():
                result = getExpressValue(tAnimationCurve)
                firstFrame = result[0]
                firstFrameValue = result[1]
                lastFrame = result[2]
                lastFrameValue = result[3]
            else:
                if len(tAnimationCurve.keys()) > 1:
                    keysCount = tAnimationCurve.size()
                    firstFrame = tAnimationCurve.keys()[0].x
                    lastFrame = tAnimationCurve.keys()[keysCount - 1].x
                    firstFrameValue = tAnimationCurve.keys()[0].y
                    lastFrameValue = tAnimationCurve.keys()[keysCount - 1].y
                else:
                    continue

            tAnimation.setAnimated(tIndex)


            if tAnimation.isAnimated():
                tAnimationCurve.clear()

            tAnimationCurve.setExpression(setCurveExpression(firstFrame, lastFrame, firstFrameValue, lastFrameValue, tweenType, easeType))









