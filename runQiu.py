import os
# For incompletely cracked NUKE, you need to set NUKE_INTERACTIVE to a non-zero value (must be a string)
os.environ["NUKE_INTERACTIVE"] = "1"

import json

# NUKE can't introduce nuke package without cracking, and it can't connect to service when it runs.
import nuke

def exportVideo():
    f = open("/Server/Nuke/1.txt", "w")
    f.write("Test")
    f.close()

    nuke.root()["last_frame"].setValue(10)
    nuke.root()["format"].setValue("1K_Super_35(full-ap)")

    # textNode = nuke.nodes.Text2(name="Text1")
    # textNode["font"].setValue('Arial', 'Regular')
    # textNode.knob("message").setValue("FANCY PANTS GORUP")
    # textNode.knob("xjustify").setValue("left")
    # textNode.knob("yjustify").setValue("bottom")
    constantNode = nuke.nodes.Constant(name="Constant1")
    constantNode.knob("color").setValue(1)


    writeNode = nuke.nodes.Write(name="Write1", file="/Server/Nuke/1.mov", file_type="mov")
    writeNode.setInput(0, constantNode)
    nuke.execute(writeNode, 1, nuke.root().lastFrame(), 1)
    print "Success!!!"

exportVideo()