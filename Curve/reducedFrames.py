import math
import time

if tn.knob("sourceText").value() == "ok" and tn.knob("targetText").value() == "ok":
    sNodeName = tn.knob("sourceNodes").value()
    sAttrMultipleName = tn.knob("attributeMultiple").value()
    sAttrSingleName = tn.knob("attributeSingle").value()
    sAttrSingleNames = tn.knob("attributeSingle").values()
    sFirstFrame = tn.knob("sourceFirstFrameX").value()
    sLastFrame = tn.knob("sourceLastFrameX").value()
    sFirstFrameY = tn.knob("sourceFirstFrameY").value()
    sLastFrameY = tn.knob("sourceLastFrameY").value()

    sDistance = sLastFrameY - sFirstFrameY
    sFrameCount = sLastFrame - sFirstFrame + 1

    curveAccuracy = float(tn.knob("CurveAccuracy").value()) * 1.0
    stepAccuracy = float(tn.knob("StepAccuracy").value()) * 1.0
    if curveAccuracy <= 0:
        curveAccuracy = 1.0
    if stepAccuracy <= 0:
        stepAccuracy = 1.0

    sIndexArr = [i for i, name in enumerate(sAttrSingleNames) if name == sAttrSingleName]
    sIndex = 0
    if len(sAttrSingleNames) > 1:
        sIndex = sIndexArr[0] - 1
    sAnimationCurve = nuke.toNode(sNodeName).knob(sAttrMultipleName).animations()[sIndex]

    isExpression = not sAnimationCurve.noExpression()

    xArr = []
    yArr = []

    flag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) > sAnimationCurve.evaluate(sFirstFrame + 0):
        flag = False
    else:
        flag = True

    equalFlag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) == sAnimationCurve.evaluate(sFirstFrame + 0):
        equalFlag = True
    else:
        equalFlag = False

    xArr.append(sFirstFrame)
    yArr.append(sAnimationCurve.evaluate(sFirstFrame))

    for i in range(int((sLastFrame - sFirstFrame) * curveAccuracy)):
        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) == 0:
            if not equalFlag:
                xArr.append(sFirstFrame + i / curveAccuracy)
                yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            equalFlag = True
        else:
            if equalFlag:
                if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) > sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy):
                    flag = False
                else:
                    flag = True
            equalFlag = False

        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) > 0:
            if not flag:
                if i / curveAccuracy != 0:
                    xArr.append(sFirstFrame + i / curveAccuracy)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            flag = True
        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) < 0:
            if flag:
                if i / curveAccuracy != 0:
                    xArr.append(sFirstFrame + i / curveAccuracy)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            flag = False

    xArr.append(sLastFrame)
    yArr.append(sAnimationCurve.evaluate(sLastFrame))


    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()
    tFirstFrame = tn.knob("targetFirstFrameX").value()
    tLastFrame = tn.knob("targetLastFrameX").value()
    tFirstFrameY = tn.knob("targetFirstFrameY").value()
    tLastFrameY = tn.knob("targetLastFrameY").value()
    coverAll = tn.knob("CoverAll").value()
    tFrameCount = tLastFrame - tFirstFrame + 1

    tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
    tIndex = 0
    if len(tAttrSingleNames) > 1:
        tIndex = tIndexArr[0] - 1
    tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)
    tAnimation.setAnimated(tIndex)

    tAnimationCurves = tAnimation.animations()
    tAnimationCurve = None
    for j in range(len(tAnimationCurves)):
        if tAnimationCurves[j].knobIndex() == tIndex:
            tAnimationCurve = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()[j]



    keysLen = len(tAnimationCurve.keys())
    isLess = keysLen > 0 and tAnimationCurve.keys()[keysLen - 1].x <= tLastFrame

    oldKeys = sAnimationCurve.keys()


    removeKeys = []
    for key in tAnimationCurve.keys():
        if isLess:
            if key.x >= tFirstFrame and key.x <= tLastFrame:
                removeKeys.append(key)
        else:
            if key.x >= tFirstFrame and key.x < tLastFrame:
                removeKeys.append(key)
    tAnimationCurve.removeKey(removeKeys)


    tYArr = []
    tXArr = []

    for i in range(len(xArr) - 1):
        if i == 0:
            tXArr.append(tFirstFrame)
        else:
            tXArr.append((xArr[i] - xArr[0]) / (sFrameCount - 1) * (tFrameCount - 1) + tFirstFrame)
    tXArr.append(tLastFrame)


    tDistance = tLastFrameY - tFirstFrameY

    for j in range(len(yArr)):
        tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstFrameY)

    keyArr = []
    keyValueArr = []
    curvatureArr = []
    for i in range(len(oldKeys)):
        curvatureObj = {}
        curvatureObj["la"] = oldKeys[i].la
        curvatureObj["lslope"] = oldKeys[i].lslope
        curvatureObj["ra"] = oldKeys[i].ra
        curvatureObj["rslope"] = oldKeys[i].rslope
        curvatureArr.append(curvatureObj)

    for i in range(len(tXArr) - 1):
        for j in range(len(oldKeys)):
            if oldKeys[j].x >= xArr[i] and oldKeys[j].x < xArr[i + 1]:
                keyArr.append((oldKeys[j].x - xArr[i]) / (xArr[i + 1] - xArr[i]) * (tXArr[i + 1] - tXArr[i]) + tXArr[i])
                keyValueArr.append((oldKeys[j].y - yArr[i]) / (yArr[i + 1] - yArr[i]) * (tYArr[i + 1] - tYArr[i]) + tYArr[i])

    keyArr.append(tLastFrame)
    keyValueArr.append(tLastFrameY)

    for i in range(len(keyArr)):
        tAnimation.setValueAt(keyValueArr[i], keyArr[i], tIndex)

    for i in range(len(curvatureArr)):
        tAnimationCurve.keys()[i].la = curvatureArr[i]["la"]
        tAnimationCurve.keys()[i].lslope = curvatureArr[i]["lslope"]
        tAnimationCurve.keys()[i].ra = curvatureArr[i]["ra"]
        tAnimationCurve.keys()[i].rslope = curvatureArr[i]["rslope"]


    # removeKeyObj = tAnimationCurve.keys()[0]
    # x = removeKeyObj.x
    # y = removeKeyObj.y
    # la = removeKeyObj.la
    # lslope = removeKeyObj.lslope
    # ra = removeKeyObj.ra
    # rslope = removeKeyObj.rslope
    # tAnimationCurve.removeKey([removeKeyObj])
    #
    # tAnimation.setValueAt(y, x, tIndex)
    # tAnimationCurve.keys()[0].la = la
    # tAnimationCurve.keys()[0].lslope = lslope
    # tAnimationCurve.keys()[0].ra = ra
    # tAnimationCurve.keys()[0].rslope = rslope





