'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-13 14:57:18
@LastEditTime: 2019-08-22 09:21:00
@LastEditors: Please set LastEditors
'''
# nuke.thisNode().knob("knobChanged").setValue('''

tn = nuke.thisNode()
tk = nuke.thisKnob()

def getMinMaxValue(firstFrame, lastFrame, animationCurve):
    minValueX = firstFrame
    minValue = animationCurve.evaluate(firstFrame)
    maxValue = animationCurve.evaluate(firstFrame)
    maxValueX = firstFrame
    frameCount = int(lastFrame - firstFrame + 1)
    for i in range(frameCount):
        value = animationCurve.evaluate(i + firstFrame)
        if value > maxValue:
            maxValue = value
            maxValueX = i + firstFrame
        if value < minValue:
            minValue = value
            minValueX = i + firstFrame

    return [minValueX, minValue, maxValueX, maxValue]


def setSourceInfo(animationCurve, type, type1):
    if type == 0:
        keysCount = animationCurve.size()
        firstFrame = animationCurve.keys()[0].x
        lastFrame = animationCurve.keys()[keysCount - 1].x
    if type == 1:
        express = animationCurve.expression()
        count = 0
        index = 0
        for i in express:
            if count == 2:
                break
            else:
                if i == ":":
                    count += 1
            index += 1

        strArr = express[0:index - 2].replace("frame", "").replace("<", "").replace(">", "").replace(" ", "").split(
            ":")
        frameInfoArr = []
        for str in strArr:
            currStrArr = str.split("?")
            frameInfoArr.append(int(float(currStrArr[0])))
            frameInfoArr.append(float(currStrArr[1]))

        firstFrame = frameInfoArr[0]
        lastFrame = frameInfoArr[2]

    tn.knob("sourceStartFrame").setValue("%d" %firstFrame)
    tn.knob("sourceEndFrame").setValue("%d" %lastFrame)
    if type1 == 0:
        tn.knob("sourceFirstFrameX").setValue(firstFrame)
        tn.knob("sourceLastFrameX").setValue(lastFrame)
    if type1 == 1:
        firstFrame = int(tn.knob("sourceFirstFrameX").value())
        lastFrame = int(tn.knob("sourceLastFrameX").value())
    tn.knob("sourceFirstFrameY").setValue(animationCurve.evaluate(firstFrame))
    tn.knob("sourceLastFrameY").setValue(animationCurve.evaluate(lastFrame))
    tn.knob("sourceText").setValue("ok")


def getSourceInfo(type):
    sourceNodeName = tn.knob("sourceNodes").value()
    attributeMultipleName = tn.knob("attributeMultiple").value()
    attributeSingleName = tn.knob("attributeSingle").value()
    attributeSingleNames = tn.knob("attributeSingle").values()
    if sourceNodeName != "None" and attributeMultipleName != "None" and attributeSingleName != "None":
        indexArr = [i for i, name in enumerate(attributeSingleNames) if name == attributeSingleName]
        index = indexArr[0] - 1
        animationKnob = nuke.toNode(sourceNodeName).knob(attributeMultipleName)
        animationCurve = animationKnob.animations()[index]

        if len(animationCurve.keys()) > 1:
            setSourceInfo(animationCurve, 0, type)
        elif animationKnob.hasExpression() and not animationCurve.noExpression():
            setSourceInfo(animationCurve, 1, type)
        else:
            tn.knob("sourceText").setValue("目标曲线的关键帧数量至少要有两个！！！")
    elif sourceNodeName != "None" and attributeMultipleName != "None" and len(attributeSingleNames) <= 1:
        animationCurve = nuke.toNode(sourceNodeName).knob(attributeMultipleName).animations()[0]
        if len(animationCurve.keys()) > 1:
            setSourceInfo(animationCurve, 0, type)
        else:
            tn.knob("sourceText").setValue("目标曲线的关键帧数量至少要有两个！！！")
    else:
        tn.knob("sourceFirstFrameX").setValue(0)
        tn.knob("sourceLastFrameX").setValue(0)
        tn.knob("sourceFirstFrameY").setValue(0)
        tn.knob("sourceLastFrameY").setValue(0)
        tn.knob("sourceStartFrame").setValue("0")
        tn.knob("sourceEndFrame").setValue("0")
        tn.knob("sourceText").setValue("没有选择源曲线！！！")


attributeMultiple = []
attributeSingle = []

if tk.name() == "sourceNodes":
    nodeName = tk.value()
    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "" and nuke.toNode(nodeName)[knobName].isAnimated():
                attributeMultiple.append(knobName)
        attributeMultiple.sort()
        attributeMultiple.insert(0, "None")
        tn.knob("attributeMultiple").setValues(attributeMultiple)
    else:
        tn.knob("attributeMultiple").setValues(["None"])

    tn.knob("attributeSingle").setValues(["None"])
    tn.knob("attributeMultiple").setValue(0)
    tn.knob("attributeSingle").setValue(0)

    getSourceInfo(0)

if tk.name() == "attributeMultiple":
    multipleAttrName = tk.value()
    if multipleAttrName != "None":
        nodeName = tn.knob("sourceNodes").value()
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.animations())):
                    attributeSingle.append(multipleAttrKnobs.animations()[i].knobAndFieldName().split(".")[1])
                attributeSingle.insert(0, "None")
                tn.knob("attributeSingle").setValues(attributeSingle)
            else:
                tn.knob("attributeSingle").setValues(["None"])
        else:
            tn.knob("attributeSingle").setValues(["None"])
    else:
        tn.knob("attributeSingle").setValues(["None"])
    tn.knob("attributeSingle").setValue(0)

    getSourceInfo(0)

if tk.name() == "attributeSingle":
    getSourceInfo(0)

if tk.name() == "sourceFirstFrameX" or tk.name() == "sourceLastFrameX":
    startFrame = int(tn.knob("sourceStartFrame").value())
    endFrame = int(tn.knob("sourceEndFrame").value())

    firstFrame = int(tn.knob("sourceFirstFrameX").value())
    lastFrame = int(tn.knob("sourceLastFrameX").value())

    if firstFrame < startFrame or firstFrame >= endFrame or lastFrame <= startFrame or lastFrame > endFrame:
        tn.knob("sourceText").setValue("设置的帧数超出了范围！！！")
    elif firstFrame >= lastFrame:
        tn.knob("sourceText").setValue("起始帧不能大于或等于结束帧！！！")
    else:
        getSourceInfo(1)





def hidePasteFrameInfo():
    tn.knob("targetFirstFrameX").setEnabled(False)
    tn.knob("targetLastFrameX").setEnabled(False)
    tn.knob("targetFirstFrameY").setEnabled(False)
    tn.knob("targetLastFrameY").setEnabled(False)
def showPasteFrameInfo():
    tn.knob("targetFirstFrameX").setEnabled(True)
    tn.knob("targetLastFrameX").setEnabled(True)
    tn.knob("targetFirstFrameY").setEnabled(True)
    tn.knob("targetLastFrameY").setEnabled(True)

def autoGetTargetValue(tNodeName, tAttrMultipleName, tAttrSingleName, tAttrSingleNames):
    firstFrame = 0
    lastFrame = 0

    tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
    tIndex = 0
    if len(tAttrSingleNames) > 1:
        tIndex = tIndexArr[0] - 1
    tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)
    # 需要设置动画，否则会报错
    tAnimation.setAnimated(tIndex)
    tAnimationCurve = tAnimation.animations()[tIndex]

    if tAnimation.isAnimated() and (tAnimation.animation(tIndex) != None) and len(tAnimation.animation(tIndex).keys()) > 1:
        tAnimationCurve = tAnimation.animation(tIndex)
        tkeysCount = len(tAnimationCurve.keys())
        firstFrame = tAnimationCurve.keys()[0].x
        lastFrame = tAnimationCurve.keys()[tkeysCount - 1].x
        
    if tAnimation.hasExpression() and tAnimation.hasExpression(tIndex):

        express = tAnimation.animation(tIndex).expression()

        count = 0
        index = 0
        for i in express:
            if count == 2:
                break
            else:
                if i == ":":
                    count += 1
            index += 1

        strArr = express[0:index - 2].replace("frame", "").replace("<", "").replace(">", "").replace(" ", "").split(
            ":")
        frameInfoArr = []
        for str in strArr:
            currStrArr = str.split("?")
            frameInfoArr.append(int(float(currStrArr[0])))

        firstFrame = frameInfoArr[0]
        lastFrame = frameInfoArr[1]

    if lastFrame > firstFrame:
        targetFirstFrame = tn.knob("targetFirstFrameX").value()
        targetLastFrame = tn.knob("targetLastFrameX").value()
        if tk.name() == "targetFirstFrameX":
            if targetFirstFrame >= firstFrame and targetFirstFrame <= lastFrame:
                tn.knob("targetFirstFrameY").setValue(tAnimationCurve.evaluate(targetFirstFrame))
            # else:
            #     tn.knob("targetFirstFrameY").setValue(0)
        if tk.name() == "targetLastFrameX":
            if targetLastFrame >= firstFrame and targetLastFrame <= lastFrame:
                tn.knob("targetLastFrameY").setValue(tAnimationCurve.evaluate(targetLastFrame))
            # else:
            #     tn.knob("targetLastFrameY").setValue(0)
    



def chooiceTargetInfo():
    targetNodeName = tn.knob("targetNodes").value()
    targetAttributeMultipleName = tn.knob("targetAttributeMultiple").value()
    targetAttributeSingleName = tn.knob("targetAttributeSingle").value()
    targetAttributeSingleNames = tn.knob("targetAttributeSingle").values()

    targetFirstFrame = tn.knob("targetFirstFrameX").value()
    targetLastFrame = tn.knob("targetLastFrameX").value()
    # targetFirstFrameValue = tn.knob("targetFirstFrameY").value()
    # targetLastFrameValue = tn.knob("targetLastFrameY").value()

    isExpression = tn.knob("IsExpression").value()
    coverAll = tn.knob("CoverAll").value()
    isExpressionFlag = False
    if isExpression == "no":
        isExpressionFlag = False
    if isExpression == "yes":
        isExpressionFlag = True

    # and targetFirstFrameValue != targetLastFrameValue
    if targetNodeName != "None" and targetAttributeMultipleName != "None" and targetAttributeSingleName != "None":
        if targetAttributeSingleName.lower() == "all":
            if not coverAll:
                tn.knob("targetText").setValue("全选状态下，必须选取Cover All选项！！！")
            else:
                tn.knob("targetText").setValue("ok")
            hidePasteFrameInfo()
        else:
            showPasteFrameInfo()
            if tk.name() == "targetFirstFrameX" or tk.name() == "targetLastFrameX":
                autoGetTargetValue(targetNodeName, targetAttributeMultipleName, targetAttributeSingleName, targetAttributeSingleNames)
            if targetLastFrame > targetFirstFrame:
                if isExpressionFlag and not coverAll:
                    tn.knob("targetText").setValue("目标曲线有表达式，必须选取Cover All选项！！！")
                else:
                    tn.knob("targetText").setValue("ok")

            else:
                tn.knob("targetText").setValue("目标曲线的结束帧要大于起始帧！！！")
                if isExpressionFlag and not coverAll:
                    tn.knob("targetText").setValue("目标曲线有表达式，必须选取Cover All选项！！！")
    elif targetNodeName != "None" and targetAttributeMultipleName != "None" and len(targetAttributeSingleNames) <= 1:
        showPasteFrameInfo()
        if tk.name() == "targetFirstFrameX" or tk.name() == "targetLastFrameX":
            autoGetTargetValue(targetNodeName, targetAttributeMultipleName, targetAttributeSingleName, targetAttributeSingleNames)
        if targetLastFrame > targetFirstFrame:
            if isExpressionFlag and not coverAll:
                tn.knob("targetText").setValue("目标曲线有表达式，必须选取Cover All选项！！！")
            else:
                tn.knob("targetText").setValue("ok")

        else:
            tn.knob("targetText").setValue("目标曲线的结束帧要大于起始帧！！！")
            if isExpressionFlag and not coverAll:
                tn.knob("targetText").setValue("目标曲线有表达式，必须选取Cover All选项！！！")
    else:
        showPasteFrameInfo()
        tn.knob("targetText").setValue("信息填写不完整！！！")
        if isExpressionFlag and not coverAll:
            tn.knob("targetText").setValue("目标曲线有表达式，必须选取Cover All选项！！！")


def hideTargetInfo():
    tn.knob("targetFirstFrameX").setValue(0)
    tn.knob("targetFirstFrameY").setValue(0)
    tn.knob("targetLastFrameX").setValue(0)
    tn.knob("targetLastFrameY").setValue(0)
    tn.knob("IsExpression").setValue("no")


def getTargetInfo():
    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()

    if (tNodeName != "None" and tAttrMultipleName != "None" and tAttrSingleName != "None") or (tNodeName != "None" and tAttrMultipleName != "None" and len(tAttrSingleNames) <= 1):
        if tAttrSingleName.lower() == "all":
            tn.knob("targetFirstFrameX").setValue(0)
            tn.knob("targetFirstFrameY").setValue(0)
            tn.knob("targetLastFrameX").setValue(0)
            tn.knob("targetLastFrameY").setValue(0)
        else:
            tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
            tIndex = 0
            if len(tAttrSingleNames) > 1:
                tIndex = tIndexArr[0] - 1
            tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

            if tAnimation.isAnimated() and (tAnimation.animation(tIndex) != None) and len(tAnimation.animation(tIndex).keys()) > 1:
                tAnimationCurve = tAnimation.animation(tIndex)
                tkeysCount = len(tAnimationCurve.keys())
                tn.knob("targetFirstFrameX").setValue(tAnimationCurve.keys()[0].x)
                tn.knob("targetFirstFrameY").setValue(tAnimationCurve.keys()[0].y)
                tn.knob("targetLastFrameX").setValue(tAnimationCurve.keys()[tkeysCount - 1].x)
                tn.knob("targetLastFrameY").setValue(tAnimationCurve.keys()[tkeysCount - 1].y)
                tn.knob("IsExpression").setValue("no")
            else:
                hideTargetInfo()
            if tAnimation.hasExpression() and tAnimation.hasExpression(tIndex):

                express = tAnimation.animation(tIndex).expression()

                count = 0
                index = 0
                for i in express:
                    if count == 2:
                        break
                    else:
                        if i == ":":
                            count += 1
                    index += 1

                strArr = express[0:index - 2].replace("frame", "").replace("<", "").replace(">", "").replace(" ", "").split(
                    ":")
                frameInfoArr = []
                for str in strArr:
                    currStrArr = str.split("?")
                    frameInfoArr.append(int(float(currStrArr[0])))
                    frameInfoArr.append(float(currStrArr[1]))

                tn.knob("targetFirstFrameX").setValue(frameInfoArr[0])
                tn.knob("targetFirstFrameY").setValue(frameInfoArr[1])
                tn.knob("targetLastFrameX").setValue(frameInfoArr[2])
                tn.knob("targetLastFrameY").setValue(frameInfoArr[3])
                tn.knob("IsExpression").setValue("yes")
    else:
        hideTargetInfo()


    chooiceTargetInfo()


targetAttributeMultiple = []
targetAttributeSingle = []

if tk.name() == "targetNodes":
    nodeName = tk.value()
    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "":
                targetAttributeMultiple.append(knobName)
        targetAttributeMultiple.sort()
        targetAttributeMultiple.insert(0, "None")
        tn.knob("targetAttributeMultiple").setValues(targetAttributeMultiple)
    else:
        tn.knob("targetAttributeMultiple").setValues(["None"])

    tn.knob("targetAttributeSingle").setValues(["None"])
    tn.knob("targetAttributeMultiple").setValue(0)
    tn.knob("targetAttributeSingle").setValue(0)

    getTargetInfo()

if tk.name() == "targetAttributeMultiple":
    multipleAttrName = tk.value()
    if multipleAttrName != "None":
        nodeName = tn.knob("targetNodes").value()
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.vect())):
                    targetAttributeSingle.append(multipleAttrKnobs.names(i))
                targetAttributeSingle.insert(0, "None")
                targetAttributeSingle.append("All")
                tn.knob("targetAttributeSingle").setValues(targetAttributeSingle)
            else:
                tn.knob("targetAttributeSingle").setValues(["None"])
        else:
            tn.knob("targetAttributeSingle").setValues(["None"])
    else:
        tn.knob("targetAttributeSingle").setValues(["None"])
    tn.knob("targetAttributeSingle").setValue(0)

    getTargetInfo()

if tk.name() == "targetAttributeSingle":
    getTargetInfo()

# if tk.name() == "targetFirstFrameX" or tk.name() == "targetFirstFrameY" or tk.name() == "targetLastFrameX" or tk.name() == "targetLastFrameY" or tk.name() == "CoverAll":
#     chooiceTargetInfo()
if tk.name() == "targetFirstFrameX" or tk.name() == "targetLastFrameX" or tk.name() == "CoverAll":
    chooiceTargetInfo()


if tn.knob("sourceText").value() == "ok" and tn.knob("targetText").value() == "ok":
    tn.knob("pasteCurve").setEnabled(True)
else:
    tn.knob("pasteCurve").setEnabled(False)







def hideEaseFrameInfo():
    tn.knob("EaseFirstFrame").setEnabled(False)
    tn.knob("EaseLastFrame").setEnabled(False)
    tn.knob("EaseFirstFrameValue").setEnabled(False)
    tn.knob("EaseLastFrameValue").setEnabled(False)
def showEaseFrameInfo():
    tn.knob("EaseFirstFrame").setEnabled(True)
    tn.knob("EaseLastFrame").setEnabled(True)
    tn.knob("EaseFirstFrameValue").setEnabled(True)
    tn.knob("EaseLastFrameValue").setEnabled(True)


def chooiceEaseTargetInfo():
    targetNodeName = tn.knob("EaseTargetNodes").value()
    targetAttributeMultipleName = tn.knob("EaseAttributeMultiple").value()
    targetAttributeSingleName = tn.knob("EaseAttributeSingle").value()
    targetAttributeSingleNames = tn.knob("EaseAttributeSingle").values()

    targetFirstFrame = tn.knob("EaseFirstFrame").value()
    targetLastFrame = tn.knob("EaseLastFrame").value()
    targetFirstFrameValue = tn.knob("EaseFirstFrameValue").value()
    targetLastFrameValue = tn.knob("EaseLastFrameValue").value()

    if targetNodeName != "None" and targetAttributeMultipleName != "None" and targetAttributeSingleName != "None":
        if targetAttributeSingleName.lower() == "all":
            tn.knob("EaseState").setValue("ok")
            hideEaseFrameInfo()
        else:
            showEaseFrameInfo()
            if targetLastFrame > targetFirstFrame and targetFirstFrameValue != targetLastFrameValue:
                tn.knob("EaseState").setValue("ok")
            else:
                tn.knob("EaseState").setValue("信息填写不完整！！！")
    elif targetNodeName != "None" and targetAttributeMultipleName != "None" and len(targetAttributeSingleNames) <= 1:
        showEaseFrameInfo()
        if targetLastFrame > targetFirstFrame and targetFirstFrameValue != targetLastFrameValue:
            tn.knob("EaseState").setValue("ok")
        else:
            tn.knob("EaseState").setValue("信息填写不完整！！！")
    else:
        showEaseFrameInfo()
        tn.knob("EaseState").setValue("信息填写不完整！！！")


def setEaseTargetInfo():
    tn.knob("EaseFirstFrame").setValue(0)
    tn.knob("EaseFirstFrameValue").setValue(0)
    tn.knob("EaseLastFrame").setValue(0)
    tn.knob("EaseLastFrameValue").setValue(0)


def getEaseTargetInfo():
    tNodeName = tn.knob("EaseTargetNodes").value()
    tAttrMultipleName = tn.knob("EaseAttributeMultiple").value()
    tAttrSingleName = tn.knob("EaseAttributeSingle").value()
    tAttrSingleNames = tn.knob("EaseAttributeSingle").values()

    if (tNodeName != "None" and tAttrMultipleName != "None" and tAttrSingleName != "None") or (tNodeName != "None" and tAttrMultipleName != "None" and len(tAttrSingleNames) <= 1):
        tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
        tIndex = 0
        if len(tAttrSingleNames) > 1:
            tIndex = tIndexArr[0] - 1
        tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

        if tAnimation.isAnimated() and (tAnimation.animation(tIndex) != None) and len(tAnimation.animation(tIndex).keys()) > 1:
            tAnimationCurve = tAnimation.animation(tIndex)
            tkeysCount = len(tAnimationCurve.keys())
            tn.knob("EaseFirstFrame").setValue(tAnimationCurve.keys()[0].x)
            tn.knob("EaseFirstFrameValue").setValue(tAnimationCurve.keys()[0].y)
            tn.knob("EaseLastFrame").setValue(tAnimationCurve.keys()[tkeysCount - 1].x)
            tn.knob("EaseLastFrameValue").setValue(tAnimationCurve.keys()[tkeysCount - 1].y)
        elif tAnimation.hasExpression() and tAnimation.hasExpression(tIndex):

            express = tAnimation.animation(tIndex).expression()

            count = 0
            index = 0
            for i in express:
                if count == 2:
                    break
                else:
                    if i == ":":
                        count += 1
                index += 1

            strArr = express[0:index - 2].replace("frame", "").replace("<", "").replace(">", "").replace(" ", "").split(
                ":")
            frameInfoArr = []
            for str in strArr:
                currStrArr = str.split("?")
                frameInfoArr.append(int(float(currStrArr[0])))
                frameInfoArr.append(float(currStrArr[1]))

            tn.knob("EaseFirstFrame").setValue(frameInfoArr[0])
            tn.knob("EaseFirstFrameValue").setValue(frameInfoArr[1])
            tn.knob("EaseLastFrame").setValue(frameInfoArr[2])
            tn.knob("EaseLastFrameValue").setValue(frameInfoArr[3])
        else:
            setEaseTargetInfo()
    else:
        setEaseTargetInfo()


    chooiceEaseTargetInfo()


easeAttributeMultiple = []
easeAttributeSingle = []

if tk.name() == "EaseTargetNodes":
    nodeName = tk.value()
    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "":
                easeAttributeMultiple.append(knobName)
        easeAttributeMultiple.sort()
        easeAttributeMultiple.insert(0, "None")
        tn.knob("EaseAttributeMultiple").setValues(easeAttributeMultiple)
    else:
        tn.knob("EaseAttributeMultiple").setValues(["None"])

    tn.knob("EaseAttributeSingle").setValues(["None"])
    tn.knob("EaseAttributeMultiple").setValue(0)
    tn.knob("EaseAttributeSingle").setValue(0)

    getEaseTargetInfo()

if tk.name() == "EaseAttributeMultiple":
    multipleAttrName = tk.value()
    if multipleAttrName != "None":
        nodeName = tn.knob("EaseTargetNodes").value()
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.vect())):
                    easeAttributeSingle.append(multipleAttrKnobs.names(i))
                easeAttributeSingle.insert(0, "None")
                easeAttributeSingle.append("All")
                tn.knob("EaseAttributeSingle").setValues(easeAttributeSingle)
            else:
                tn.knob("EaseAttributeSingle").setValues(["None"])
        else:
            tn.knob("EaseAttributeSingle").setValues(["None"])
    else:
        tn.knob("EaseAttributeSingle").setValues(["None"])
    tn.knob("EaseAttributeSingle").setValue(0)

    getEaseTargetInfo()

if tk.name() == "EaseAttributeSingle":
    getEaseTargetInfo()

if tk.name() == "EaseFirstFrame" or tk.name() == "EaseFirstFrameValue" or tk.name() == "EaseLastFrame" or tk.name() == "EaseLastFrameValue":
    chooiceEaseTargetInfo()

if tn.knob("EaseState").value() == "ok":
    tn.knob("EaseSetEase").setEnabled(True)
else:
    tn.knob("EaseSetEase").setEnabled(False)


# ''')
