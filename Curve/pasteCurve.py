'''
@Description: In User Settings Edit
@Author: your name
@Date: 2019-08-13 14:57:18
@LastEditTime: 2019-08-21 15:11:29
@LastEditors: Please set LastEditors
'''
import math


def getExpressValueForPaste(animationCurve):
    express = animationCurve.expression()
    count = 0
    index = 0
    for i in express:
        if count == 2:
            break
        else:
            if i == ":":
                count += 1
        index += 1

    strArr = express[0:index - 2].replace("frame", "").replace("<", "").replace(">", "").replace(" ", "").split(":")
    frameInfoArr = []
    for str in strArr:
        currStrArr = str.split("?")
        frameInfoArr.append(int(float(currStrArr[0])))
        frameInfoArr.append(float(currStrArr[1]))

    return [frameInfoArr[0], frameInfoArr[1], frameInfoArr[2], frameInfoArr[3]]



if tn.knob("sourceText").value() == "ok" and tn.knob("targetText").value() == "ok":
    sNodeName = tn.knob("sourceNodes").value()
    sAttrMultipleName = tn.knob("attributeMultiple").value()
    sAttrSingleName = tn.knob("attributeSingle").value()
    sAttrSingleNames = tn.knob("attributeSingle").values()
    sFirstFrame = tn.knob("sourceFirstFrameX").value()
    sLastFrame = tn.knob("sourceLastFrameX").value()
    # sFirstFrameY = tn.knob("sourceFirstFrameY").value()
    # sLastFrameY = tn.knob("sourceLastFrameY").value()

    curveAccuracy = float(tn.knob("CurveAccuracy").value()) * 1.0
    stepAccuracy = float(tn.knob("StepAccuracy").value()) * 1.0
    if curveAccuracy <= 0:
        curveAccuracy = 1.0
    if stepAccuracy <= 0:
        stepAccuracy = 1.0

    sIndexArr = [i for i, name in enumerate(sAttrSingleNames) if name == sAttrSingleName]
    sIndex = 0
    if len(sAttrSingleNames) > 1:
        sIndex = sIndexArr[0] - 1
    sAnimationCurve = nuke.toNode(sNodeName).knob(sAttrMultipleName).animations()[sIndex]

    sFirstFrameY = sAnimationCurve.evaluate(sFirstFrame)
    sLastFrameY = sAnimationCurve.evaluate(sLastFrame)

    sDistance = sLastFrameY - sFirstFrameY
    sFrameCount = sLastFrame - sFirstFrame + 1

    # 是否有表达式存在
    sHaveExpression = not sAnimationCurve.noExpression()

    xArr = []
    yArr = []

    flag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) > sAnimationCurve.evaluate(sFirstFrame + 0):
        flag = False
    else:
        flag = True

    equalFlag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) == sAnimationCurve.evaluate(sFirstFrame + 0):
        equalFlag = True
    else:
        equalFlag = False

    xArr.append(sFirstFrame)
    yArr.append(sAnimationCurve.evaluate(sFirstFrame))

    for i in range(int((sLastFrame - sFirstFrame) * curveAccuracy)):
        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) == 0:
            if not equalFlag:
                xArr.append(sFirstFrame + i / curveAccuracy)
                yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            equalFlag = True
        else:
            if equalFlag:
                if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) > sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy):
                    flag = False
                else:
                    flag = True
            equalFlag = False

        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) > 0:
            if not flag:
                if i / curveAccuracy != 0:
                    xArr.append(sFirstFrame + i / curveAccuracy)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            flag = True
        if sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy + 1 / curveAccuracy) - sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy) < 0:
            if flag:
                if i / curveAccuracy != 0:
                    xArr.append(sFirstFrame + i / curveAccuracy)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i / curveAccuracy))
            flag = False

    xArr.append(sLastFrame)
    yArr.append(sAnimationCurve.evaluate(sLastFrame))



    grid = None
    nodes = nuke.thisNode().nodes()
    for node in nodes:
        if node.name() == "Grid1":
            grid = node
            break
    
    gAnimation = grid.knob("translate")
    gAnimation.clearAnimated(0)
    gAnimation.setAnimated(0)
    
    # for m in range(int(math.ceil((sLastFrame - sFirstFrame + 1) * stepAccuracy))):
    for m in range(int(math.ceil(sLastFrame - sFirstFrame + 1))):
        frameX = sFirstFrame + m
        gAnimation.setValueAt( sAnimationCurve.evaluate(frameX), frameX, 0)


    gAnimationCurves = gAnimation.animations()
    gAnimationCurve = None
    for n in range(len(gAnimationCurves)):
        if gAnimationCurves[n].knobIndex() == 0:
            gAnimationCurve = gAnimation.animations()[n]




    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()
    tFirstFrame = tn.knob("targetFirstFrameX").value()
    tLastFrame = tn.knob("targetLastFrameX").value()
    tFirstFrameY = tn.knob("targetFirstFrameY").value()
    tLastFrameY = tn.knob("targetLastFrameY").value()
    coverAll = tn.knob("CoverAll").value()

    tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
    tIndex = 0
    isSingle = True
    if tAttrSingleName.lower() == "all":
        isSingle = False
    if len(tAttrSingleNames) > 1:
        tIndex = tIndexArr[0] - 1

    tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

    num = 1
    if len(tAttrSingleNames) > 1 and tIndexArr[0] == len(tAttrSingleNames) - 1:
        num = len(tAttrSingleNames) - 1 - 1
    for n in range(num):
        if isSingle:
            if n == 1:
                break
        if num != 1:
            tIndex = n

    
        firstFrame = tFirstFrame
        lastFrame = tLastFrame
        firstFrameValue = tFirstFrameY
        lastFrameValue = tLastFrameY
        
        tAnimationCurves = tAnimation.animations()
        tAnimationCurve = None
        for j in range(len(tAnimationCurves)):
            if tAnimationCurves[j].knobIndex() == tIndex:
                tAnimationCurve = tAnimationCurves[j]

        if tAnimationCurve == None and not isSingle:
            continue


        if not isSingle:
            if not tAnimationCurve.noExpression():
                result = getExpressValueForPaste(tAnimationCurve)
                firstFrame = result[0]
                firstFrameValue = result[1]
                lastFrame = result[2]
                lastFrameValue = result[3]
            else:
                if len(tAnimationCurve.keys()) > 1:
                    keysCount = tAnimationCurve.size()
                    firstFrame = tAnimationCurve.keys()[0].x
                    lastFrame = tAnimationCurve.keys()[keysCount - 1].x
                    firstFrameValue = tAnimationCurve.keys()[0].y
                    lastFrameValue = tAnimationCurve.keys()[keysCount - 1].y
                else:
                    continue

        tDistance = lastFrameValue - firstFrameValue
        tFrameCount = lastFrame - firstFrame + 1


        # 是否有表达式存在
        tHaveExpression = tAnimation.hasExpression(tIndex)

        if coverAll or tHaveExpression:
            tAnimation.clearAnimated(tIndex)
            # if tAnimation.isAnimated():
                # tAnimationCurve.clear()

        tAnimation.setAnimated(tIndex)

        # 需要重新获取一下tAnimationCurves和tAnimationCurve
        tAnimationCurves = tAnimation.animations()
        tAnimationCurve = None
        for j in range(len(tAnimationCurves)):
            if tAnimationCurves[j].knobIndex() == tIndex:
                tAnimationCurve = tAnimationCurves[j]



        # 判断目标曲线已经存在的帧数是否少于或等于设置的目标帧数，如果小于等于则只覆盖选中帧
        keysLen = len(tAnimationCurve.keys())
        isLess = keysLen > 0 and tAnimationCurve.keys()[keysLen - 1].x <= lastFrame

        removeKeys = []
        for key in tAnimationCurve.keys():
            if isLess:
                if key.x >= firstFrame and key.x <= lastFrame:
                    removeKeys.append(key)
            else:
                if key.x >= firstFrame and key.x < lastFrame:
                    removeKeys.append(key)
        tAnimationCurve.removeKey(removeKeys)


        tYArr = []
        tXArr = []

        for i in range(len(xArr) - 1):
            if i == 0:
                tXArr.append(firstFrame)
            else:
                tXArr.append((xArr[i] - xArr[0]) / (sFrameCount - 1) * (tFrameCount - 1) + firstFrame)
        tXArr.append(lastFrame)

        multipleValue = 1
        sZeroFlag = False
        tZeroFlag = False
        if sDistance == tDistance:
            multipleValue = 1
        elif sDistance == 0 and tDistance != 0:
            sZeroFlag = True
            multipleValue = 1
        elif sDistance != 0 and tDistance == 0:
            tZeroFlag = True
            multipleValue = 1
        else:
            multipleValue = tDistance /sDistance

        fg1 = False
        fg2 = False
        for j in range(len(yArr)):
            value = (yArr[j] - yArr[0]) * multipleValue
            if sZeroFlag and j != 0:
                if yArr[j] - yArr[j - 1] != 0 and not fg1:
                    fg1 = True
                if fg1:
                    if yArr[j] - yArr[j - 1] > 0:
                        value = yArr[j] + math.fabs(tDistance)
                    else:
                        value = yArr[j] - math.fabs(tDistance)

                if j == len(yArr) - 1:
                    value = lastFrameValue - firstFrameValue


            if tZeroFlag and j != 0:
                if yArr[j] - yArr[j - 1] != 0 and not fg2:
                    fg2 = True
                if fg2:
                    if yArr[j] - yArr[j - 1] > 0:
                        value = yArr[j] + math.fabs(sDistance)
                    else:
                        value = yArr[j] - math.fabs(sDistance)

                if j == len(yArr) - 1:
                    value = lastFrameValue - firstFrameValue

            tYArr.append(value + firstFrameValue)


        for k in range(len(tYArr) - 1):
            frameRotio = (xArr[k + 1] - xArr[k]) / (tXArr[k + 1] - tXArr[k])
            for i in range(int(math.ceil((tXArr[k + 1] - tXArr[k]) * stepAccuracy))):
                if yArr[k + 1] - yArr[k] == 0:
                    tAnimation.setValueAt(tYArr[k], tXArr[k] + i / stepAccuracy, tIndex)
                else:
                    frameX = tXArr[k] + i / stepAccuracy
                    tAnimation.setValueAt( (gAnimationCurve.evaluate(xArr[k] + i / stepAccuracy * frameRotio) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k], frameX, tIndex)

        tAnimation.setValueAt(lastFrameValue, lastFrame, tIndex)
        
    # tn.knob("updateInfo").execute()


