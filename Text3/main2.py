import os
os.environ["NUKE_INTERACTIVE"] = "1"

import nuke

screenWidth = 1024
screenHeight = 778
textWidth = 300
# textWidth = 578
# textWidth = 890
text = "Fancy"
# text = "Fancy Pants"
# text = "Fancy Pants Group"
textContent = text + " "
textLength = len(textContent)
stepLength = 12
frameCount = len(text) * stepLength
moveDistance = 25

nuke.root()["last_frame"].setValue(frameCount)
nuke.root()["format"].setValue("1K_Super_35(full-ap)")

textNode = nuke.nodes.Text2(name="Text1")
# print textNode["font"].getValue()
textNode["font"].setValue('Arial', 'Regular')
textNode["box"].setValue((-screenWidth * 10, 0, screenWidth, 0))
textNode.knob("message").setValue(textContent)
textNode.knob("xjustify").setValue("right")
textNode.knob("yjustify").setValue("bottom")


textValue = 0
kernValues = "{"
for n in range(textLength):
    if n == textLength - 1:
        textValue = moveDistance
    kernValues += str(n) + " " + str(textValue) + " "
kernValues += "}"

# print kernValues

textNode.knob("kern_values").fromScript(kernValues)
textNode.knob("kerning").toScript()

kernAnimate = textNode["kern_values"]
kernAnimate.setAnimated()

moveValue = 0
for n in range(textLength):
    for m in range(textLength):
        if n == textLength - 1 - m:
            moveValue = moveDistance
        else:
            moveValue = 0
        kernAnimate.setValueAt(m, n*stepLength, m*2)
        kernAnimate.setValueAt(moveValue, n*stepLength, m*2+1)



transformNode = nuke.nodes.Transform(name="Transform1")
transformNode["translate"].setValue((-(screenWidth-textWidth) / 2, screenHeight/2))
transformNode.setInput(0, textNode)


writeNode = nuke.nodes.Write(name="Write1", file="C:/Users/FPG/Desktop/1.mov", file_type="mov")
writeNode.setInput(0, transformNode)

nuke.execute(writeNode, 1, nuke.root().lastFrame(), 1)