import nuke
tn = None

import math

if tn.knob("sourceText").value() == "ok" and tn.knob("targetText").value() == "Replicable":
    sNodeName = tn.knob("sourceNodes").value()
    sAttrMultipleName = tn.knob("attributeMultiple").value()
    sAttrSingleName = tn.knob("attributeSingle").value()
    sAttrSingleNames = tn.knob("attributeSingle").values()
    sFirstFrame = tn.knob("sourceFirstFrameX").value()
    sLastFrame = tn.knob("sourceLastFrameX").value()
    sFirstFrameY = tn.knob("sourceFirstFrameY").value()
    sLastFrameY = tn.knob("sourceLastFrameY").value()
    sDistance = sLastFrameY - sFirstFrameY
    sFrameCount = sLastFrame - sFirstFrame + 1

    sIndexArr = [i for i, name in enumerate(sAttrSingleNames) if name == sAttrSingleName]
    sIndex = 0
    if len(sAttrSingleNames) > 1:
        sIndex = sIndexArr[0] - 1
    sAnimationCurve = nuke.toNode(sNodeName).knob(sAttrMultipleName).animations()[sIndex]

    xArr = []
    yArr = []

    flag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) > sAnimationCurve.evaluate(sFirstFrame + 0):
        flag = False
    else:
        flag = True

    equalFlag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) == sAnimationCurve.evaluate(sFirstFrame + 0):
        equalFlag = True
    else:
        equalFlag = False

    xArr.append(sFirstFrame)
    yArr.append(sAnimationCurve.evaluate(sFirstFrame))

    for i in range(int(sLastFrame - sFirstFrame)):
        if sAnimationCurve.evaluate(sFirstFrame + i + 1) - sAnimationCurve.evaluate(sFirstFrame + i) == 0:
            if not equalFlag:
                xArr.append(sFirstFrame + i)
                yArr.append(sAnimationCurve.evaluate(sFirstFrame + i))
            equalFlag = True
        else:
            if equalFlag:
                if sAnimationCurve.evaluate(sFirstFrame + i + 1) > sAnimationCurve.evaluate(sFirstFrame + i):
                    flag = False
                else:
                    flag = True
            equalFlag = False

        if sAnimationCurve.evaluate(sFirstFrame + i + 1) - sAnimationCurve.evaluate(sFirstFrame + i) > 0:
            if not flag:
                if i != 0:
                    xArr.append(sFirstFrame + i)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i))
            flag = True
        if sAnimationCurve.evaluate(sFirstFrame + i + 1) - sAnimationCurve.evaluate(sFirstFrame + i) < 0:
            if flag:
                if i != 0:
                    xArr.append(sFirstFrame + i)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i))
            flag = False

    xArr.append(sLastFrame)
    yArr.append(sAnimationCurve.evaluate(sLastFrame))

    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()
    tFirstFrame = tn.knob("targetFirstFrameX").value()
    tLastFrame = tn.knob("targetLastFrameX").value()
    tFirstFrameY = tn.knob("targetFirstFrameY").value()
    tLastFrameY = tn.knob("targetLastFrameY").value()
    if not tn.knob("showUserConfiguration").value():
        tFirstFrame = tn.knob("targetFirstFrameXUser").value()
        tLastFrame = tn.knob("targetLastFrameXUser").value()
        tFirstFrameY = tn.knob("targetFirstFrameYUser").value()
        tLastFrameY = tn.knob("targetLastFrameYUser").value()
    tFrameCount = tLastFrame - tFirstFrame + 1

    tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
    tIndex = 0
    if len(tAttrSingleNames) > 1:
        tIndex = tIndexArr[0] - 1
    tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)
    tAnimation.setAnimated(tIndex)

    tAnimationCurves = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()
    tAnimationCurve = None
    for j in range(len(tAnimationCurves)):
        if tAnimationCurves[j].knobIndex() == tIndex:
            tAnimationCurve = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()[j]

    frameMultiple = (sFrameCount - 0) / (tFrameCount - 0) * 1.0
    frameMultiple2 = (sFrameCount - 1) / (tFrameCount - 1) * 1.0

    tYArr = []
    tXArr = []

    for i in range(len(xArr) - 1):
        if i == 0:
            tXArr.append(tFirstFrame)
        else:
            # tXArr.append(xArr[i] / frameMultiple + tFirstFrame)
            tXArr.append(round((xArr[i] - xArr[0]) / (sFrameCount - 1) * (tFrameCount - 1) + tFirstFrame))
    tXArr.append(tLastFrame)


    tDistance = tLastFrameY - tFirstFrameY
    stDistance = tDistance / sDistance * 1.0

    for j in range(len(yArr)):
        tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstFrameY)

    if tAnimation.isAnimated():
        tAnimationCurve.clear()
        #tAnimation.clearAnimated(tIndex)
    # tAnimationCurve.setExpression("Curve")


    for k in range(len(tYArr) - 1):
        frameRotio = (xArr[k + 1] - xArr[k]) / (tXArr[k + 1] - tXArr[k])
        for i in range(int(tXArr[k + 1] - tXArr[k])):
            if yArr[k + 1] - yArr[k] == 0:
                #tAnimationCurve.setKey(xArr[k] / frameMultiple + i, tYArr[k])
                # tAnimation.setValueAt(tYArr[k], xArr[k] / frameMultiple + i, tIndex)
                tAnimation.setValueAt(tYArr[k], tXArr[k] + i, tIndex)
            else:
                #tAnimationCurve.setKey(xArr[k] / frameMultiple + i, (sAnimationCurve.evaluate(xArr[k] + i * frameMultiple) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k])
                # if i == 0 and k == 0:
                #     frameX = tFirstFrame
                # else:
                #     frameX = xArr[k] / frameMultiple + (i - 1) + tFirstFrame
                # if k == 0:
                #     frameX = i + tFirstFrame
                # else:
                #     frameX = i + tFirstFrame + (xArr[k] - xArr[0]) / frameMultiple
                # frameX = tXArr[k] + frameRotio * i
                frameX = tXArr[k] + i
                # tAnimation.setValueAt( (sAnimationCurve.evaluate(xArr[k] + i * frameMultiple) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k], frameX, tIndex)
    #tAnimationCurve.setKey(tLastFrame, tLastFrameY)
                tAnimation.setValueAt( (sAnimationCurve.evaluate(xArr[k] + i * frameRotio) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k], frameX, tIndex)
    tAnimation.setValueAt(tLastFrameY, tLastFrame, tIndex)

    dir = -1
    if (sLastFrameY > sFirstFrameY and tLastFrameY > tFirstFrameY) or (
            sLastFrameY < sFirstFrameY and tLastFrameY < tFirstFrameY):
        dir = 1
    sKeysLen = len(sAnimationCurve.keys())
    tKeysLen = len(tAnimationCurve.keys())
    tAnimationCurve.keys()[tKeysLen - 1].la = sAnimationCurve.keys()[sKeysLen - 1].la * dir
    tAnimationCurve.keys()[tKeysLen - 1].lslope = sAnimationCurve.keys()[sKeysLen - 1].lslope * dir
    tAnimationCurve.keys()[tKeysLen - 1].ra = sAnimationCurve.keys()[sKeysLen - 1].ra * dir
    tAnimationCurve.keys()[tKeysLen - 1].rslope = sAnimationCurve.keys()[sKeysLen - 1].rslope * dir

