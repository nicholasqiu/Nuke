import nuke
import math

tn = nuke.thisNode()

if tn.knob("sourceText").value() == "Have chosen" and tn.knob("targetText").value() == "Replicable":
    sNodeName = tn.knob("sourceNodes").value()
    sAttrMultipleName = tn.knob("attributeMultiple").value()
    sAttrSingleName = tn.knob("attributeSingle").value()
    sAttrSingleNames = tn.knob("attributeSingle").values()
    sFirstFrame = tn.knob("sourceFirstFrameX").value()
    sLastFrame = tn.knob("sourceLastFrameX").value()
    sFirstFrameY = tn.knob("sourceFirstFrameY").value()
    sLastFrameY = tn.knob("sourceLastFrameY").value()
    sMin = sFirstFrameY
    sMax = sLastFrameY
    if sMin > sMax:
        sMin, sMax = sMax, sMin
    sFrameCount = sLastFrame - sFirstFrame + 1

    sIndexArr = [i for i, name in enumerate(sAttrSingleNames) if name == sAttrSingleName]
    sIndex = 0
    if len(sAttrSingleNames) > 1:
        sIndex = sIndexArr[0] - 1
    sAnimationCurve = nuke.toNode(sNodeName).knob(sAttrMultipleName).animations()[sIndex]

    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()
    tFirstFrame = tn.knob("targetFirstFrameX").value()
    tLastFrame = tn.knob("targetLastFrameX").value()
    tFirstFrameY = tn.knob("targetFirstFrameY").value()
    tLastFrameY = tn.knob("targetLastFrameY").value()
    if not tn.knob("showUserConfiguration").value():
        tFirstFrame = tn.knob("targetFirstFrameXUser").value()
        tLastFrame = tn.knob("targetLastFrameXUser").value()
        tFirstFrameY = tn.knob("targetFirstFrameYUser").value()
        tLastFrameY = tn.knob("targetLastFrameYUser").value()
    tFrameCount = tLastFrame - tFirstFrame + 1
    tMin = tFirstFrameY
    tMax = tLastFrameY
    if tFirstFrameY > tLastFrameY:
        tMin, tMax = tMax, tMin

    direction = 1
    if (sFirstFrameY > sLastFrameY and tFirstFrameY > tLastFrameY) or (sFirstFrameY < sLastFrameY and tFirstFrameY < tLastFrameY):
        direction = 1
    else:
        direction = -1


    tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
    tIndex = 0
    if len(tAttrSingleNames) > 1:
        tIndex = tIndexArr[0] - 1
    tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)
    tAnimation.setAnimated()

    tAnimationCurve = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()[tIndex]

    frameMultiple = sFrameCount / tFrameCount * 1.0

    sRange = sMax - sMin
    tRange = tMax - tMin

    if tAnimation.isAnimated():
        tAnimationCurve.clear()

    if direction >= 0:
        if frameMultiple > 1.0:
            for i in range(int(tFrameCount) - 1):
                tAnimationCurve.setKey(i + tFirstFrame, (sAnimationCurve.evaluate(sFirstFrame + i * frameMultiple) - sMin) / sRange * tRange + tMin)
        elif frameMultiple < 1.0:
            for i in range(int(sFrameCount) - 1):
                tAnimationCurve.setKey(tFirstFrame + i / frameMultiple,(sAnimationCurve.evaluate(i + sFirstFrame) - sMin) / sRange * tRange + tMin)
        else:
            for i in range(int(sFrameCount) - 1):
                firstValue = (sAnimationCurve.evaluate(0 + sFirstFrame) - sMin) / sRange * tRange + tMin
                currentValue = (sAnimationCurve.evaluate(i + sFirstFrame) - sMin) / sRange * tRange + tMin
                tAnimationCurve.setKey(i + tFirstFrame, currentValue)
    else:
        if frameMultiple > 1.0:
            for i in range(int(tFrameCount) - 1):
                flipValue = tFirstFrameY
                if sLastFrameY > sFirstFrameY:
                    flipValue = tLastFrameY
                currentValue = (sAnimationCurve.evaluate(sFirstFrame + i * frameMultiple) - sMin) / sRange * tRange + tMin
                tAnimationCurve.setKey(i + tFirstFrame, flipValue - currentValue + flipValue - (tMax - tMin) * direction)
        elif frameMultiple < 1.0:
            for i in range(int(sFrameCount) - 1):
                flipValue = tFirstFrameY
                if sLastFrameY > sFirstFrameY:
                    flipValue = tLastFrameY
                currentValue = (sAnimationCurve.evaluate(i + sFirstFrame) - sMin) / sRange * tRange + tMin
                tAnimationCurve.setKey(tFirstFrame + i / frameMultiple, 2 *flipValue - currentValue - (tMax - tMin) * direction)
        else:
            for i in range(int(sFrameCount) - 1):
                flipValue = tFirstFrameY
                if sLastFrameY > sFirstFrameY:
                    flipValue = tLastFrameY
                # firstValue = (sAnimationCurve.evaluate(0 + sFirstFrame) - sMin) / sRange * tRange + tMin
                currentValue = (sAnimationCurve.evaluate(i + sFirstFrame) - sMin) / sRange * tRange + tMin
                tAnimationCurve.setKey(i + tFirstFrame, 2 * flipValue - currentValue - (tMax - tMin) * direction)
    tAnimationCurve.setKey(tLastFrame, tLastFrameY)




















