import time
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from bs4 import BeautifulSoup
import os
os.environ["NUKE_INTERACTIVE"] = "1"

import nuke

# Text content
text = "Fan Cy Pants Gro Up"
# A separator for separating strings
splitKey = " "
# Font name(Not for the time being.)
fontFamily = "arial"
# Font size(Not for the time being.)
fontSize = 100
# letter-spacing(Not for the time being.)
letterSpacing = 0
# word-spacing(Not for the time being.)
wordSpacing = 0
# line-height(Not for the time being.)
lineHeight = 100
# Scene width and height(Need to format the scene)
screenWidth = 1024
screenHeight = 778
# Number of frames per character animated
stepLength = 20
# Segmented character array
textArr = []
if splitKey == "":
    textArr = list(text)
else:
    textArr = text.split(splitKey)
# Character array length
textArrLength = len(textArr)
# Total Frames
frameCount = (textArrLength + 1) * stepLength


fontFamilyCss = "@font-face{font-family: '"+fontFamily+"';src: url('./"+fontFamily+".ttf') format('truetype');}"

letterSpacingNuke = letterSpacing * 1.0 / fontSize * 2;
wordSpacingNuke = wordSpacing * 1.0 / fontSize * 2;


# with open('E:\pythonProject\Nuke\Text3\index.html', 'r') as f:
#     htmlContent = f.read()
#
# newHtmlContent = htmlContent.replace("<style>", "<style>"+fontFamilyCss)
# # print newHtmlContent
#
# with open('E:\pythonProject\Nuke\Text3\index.html', 'w') as f:
#     f.write(newHtmlContent)

chromeOptions = webdriver.ChromeOptions()
chromeOptions.add_argument("--headless")
browser = webdriver.Chrome(
    executable_path="C:\Users\FPG\AppData\Local\Programs\Python\Python37\Scripts\chromedriver.exe",
    options=chromeOptions)
# Create a local web server
browser.get('http://192.168.1.26/Text3/index.html')

wait = WebDriverWait(browser, 10)
wait.until(EC.presence_of_all_elements_located((By.ID, "content")))


sum = 0
textContent = ""
textArrContent = []
for n in textArr:
    if sum < textArrLength - 1:
        textContent += "<span>" + n + splitKey + "</span>"
        textArrContent.append(n + splitKey)
    else:
        textContent += "<span>" + n + "</span>"
        textArrContent.append(n)
    sum += 1

# browser.execute_script(
#     "insertTextContent('" + textContent + "');updateTextParamer('" + str(fontSize) + "', '" + str(lineHeight) + "', '" + str(letterSpacing) + "', '" + str(wordSpacing) + "', '" + str(fontFamily) + "');getTextWidth();getCharacterWidth();")
browser.execute_script(
    "insertTextContent('" + textContent + "');updateTextParamer('" + str(fontSize) + "', '" + str(lineHeight) + "', '" + str(letterSpacing) + "', '" + str(wordSpacing) + "');")
time.sleep(1)
browser.execute_script("getTextWidth();getCharacterWidth();")


html = BeautifulSoup(browser.page_source, features="html.parser")

characterWidth = html.select("#character-width")[0].text.split(",")
characterWidthArr = [int(i) for i in characterWidth]
textWidth = int(html.select("#text-width")[0].text)

characterHeight = html.select("#character-height")[0].text.split(",")
characterHeightArr = [int(i) for i in characterHeight]
textHeight = int(html.select("#text-height")[0].text)

print characterWidthArr
print textWidth
print characterHeightArr
print textHeight




nuke.root()["last_frame"].setValue(frameCount)
nuke.root()["format"].setValue("1K_Super_35(full-ap)")

mergeNode = nuke.nodes.Merge2(name="Merge1")
count = 0
inputStep = 0
for s in textArrContent:
    textNode = nuke.nodes.Text2(name="Text"+str(count+1)+"")
    textNode["font"].setValue('Arial', 'Regular')
    textNode["box"].setValue((0, 0, characterWidthArr[count], characterHeightArr[count]))
    textNode.knob("message").setValue(s)
    textNode.knob("xjustify").setValue("left")
    textNode.knob("yjustify").setValue("bottom")

    offsetCount = 0
    transformNode = nuke.nodes.Transform(name="Transform"+str(count+1)+"")
    if count == 0:
        offsetCount = 0
    else:
        sum = 0
        for i in range(count):
            sum += characterWidthArr[i]
        offsetCount = sum
    transformNode["translate"].setValue((offsetCount, 0))
    transformNode.setInput(0, textNode);


    transformNodeAnimate = nuke.nodes.Transform(name="TransformAnimate"+str(count+1)+"")
    transformNodeAnimate["translate"].setValue((-screenWidth, 0))
    transformNodeAnimate.setInput(0, transformNode);

    kTransform = transformNodeAnimate["translate"]
    kTransform.setAnimated()

    for f in range(textArrLength + 1):
        if count < textArrLength - f:
            kTransform.setValueAt(-screenWidth, f*stepLength, 0)
        else:
            # sumAnimate = 0
            # for j in range(textArrLength):
            #     if j < f:
            #         sumAnimate += characterWidthArr[textArrLength-j-1]/2.0
            # kTransform.setValueAt(sumAnimate - textWidth/2.0, f * stepLength, 0)
            kTransform.setValueAt(0, f * stepLength, 0)

        kTransform.setValueAt(0, f*stepLength, 1)

    if count == 2:
        inputStep = 1
    mergeNode.setInput(count+inputStep, transformNodeAnimate)

    count += 1


transformNode = nuke.nodes.Transform(name="Transform"+str(count+1)+"")
offsetWidth = (screenWidth-textWidth)/2.0-(textWidth/2.0-characterWidthArr[textArrLength-1]/2.0)
# offsetWidth = screenWidth/2.0-textWidth
offsetHeight = (screenHeight-textHeight)/2.0
transformNode["translate"].setValue((offsetWidth, offsetHeight))
transformNode.setInput(0, mergeNode);
kTransform = transformNode["translate"]
kTransform.setAnimated()

offsetMoveArr = []
frameWidth = screenWidth*1.0/stepLength
for i in range(textArrLength + 1):
    if i > 1:
        offsetMove = 0
        for j in range(i - 1):
            offsetMove += characterWidthArr[textArrLength - j - 2] / 2.0
        offsetMoveArr.append(offsetMove)
        kTransform.setValueAt(offsetWidth + offsetMoveArr[i-1], i * stepLength - 2.0*int(characterWidthArr[textArrLength- i]/frameWidth), 0)
        kTransform.setValueAt(offsetWidth + offsetMove, i * stepLength, 0)
        kTransform.setValueAt(offsetHeight, i * stepLength + int(offsetMove/frameWidth), 1)
    else:
        kTransform.setValueAt(offsetWidth, i * stepLength, 0)
        kTransform.setValueAt(offsetHeight, i * stepLength, 1)
        offsetMoveArr.append(0)


writeNode = nuke.nodes.Write(name="Write1", file="C:/Users/FPG/Desktop/1.mov", file_type="mov")
writeNode.setInput(0, transformNode)

nuke.execute(writeNode, 1, nuke.root().lastFrame(), 1)








