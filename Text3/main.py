import os
os.environ["NUKE_INTERACTIVE"] = "1"

import nuke

frameCount = 50
step = frameCount / 5
textContent = "Fancy"

nuke.root()["last_frame"].setValue(frameCount)
nuke.root()["format"].setValue("1K_Super_35(full-ap)")

textNode = nuke.nodes.Text2(name="Text1")
textNode.knob("message").setValue(" " + textContent)
textNode.knob("yjustify").setValue("bottom")
kernValues = "{0 0 1 0 2 0 3 0 4 0 5 0}"
textNode.knob("kern_values").fromScript(kernValues)
textNode.knob("kerning").toScript()

kernAnimate = textNode["kern_values"]
kernAnimate.setAnimated()

kernAnimate.setValueAt(0, 0, 0)
kernAnimate.setValueAt(0, 0, 1)
kernAnimate.setValueAt(1, 0, 2)
kernAnimate.setValueAt(0, 0, 3)
kernAnimate.setValueAt(2, 0, 4)
kernAnimate.setValueAt(0, 0, 5)
kernAnimate.setValueAt(3, 0, 6)
kernAnimate.setValueAt(0, 0, 7)
kernAnimate.setValueAt(4, 0, 8)
kernAnimate.setValueAt(0, 0, 9)
kernAnimate.setValueAt(5, 0, 10)
kernAnimate.setValueAt(0, 0, 11)

kernAnimate.setValueAt(0, 1*step, 0)
kernAnimate.setValueAt(0, 1*step, 1)
kernAnimate.setValueAt(1, 1*step, 2)
kernAnimate.setValueAt(0, 1*step, 3)
kernAnimate.setValueAt(2, 1*step, 4)
kernAnimate.setValueAt(0, 1*step, 5)
kernAnimate.setValueAt(3, 1*step, 6)
kernAnimate.setValueAt(0, 1*step, 7)
kernAnimate.setValueAt(4, 1*step, 8)
kernAnimate.setValueAt(0, 1*step, 9)
kernAnimate.setValueAt(5, 1*step, 10)
kernAnimate.setValueAt(14, 1*step, 11)

kernAnimate.setValueAt(0, 2*step, 0)
kernAnimate.setValueAt(0, 2*step, 1)
kernAnimate.setValueAt(1, 2*step, 2)
kernAnimate.setValueAt(0, 2*step, 3)
kernAnimate.setValueAt(2, 2*step, 4)
kernAnimate.setValueAt(0, 2*step, 5)
kernAnimate.setValueAt(3, 2*step, 6)
kernAnimate.setValueAt(0, 2*step, 7)
kernAnimate.setValueAt(4, 2*step, 8)
kernAnimate.setValueAt(14, 2*step, 9)
kernAnimate.setValueAt(5, 2*step, 10)
kernAnimate.setValueAt(0, 2*step, 11)

kernAnimate.setValueAt(0, 3*step, 0)
kernAnimate.setValueAt(0, 3*step, 1)
kernAnimate.setValueAt(1, 3*step, 2)
kernAnimate.setValueAt(0, 3*step, 3)
kernAnimate.setValueAt(2, 3*step, 4)
kernAnimate.setValueAt(0, 3*step, 5)
kernAnimate.setValueAt(3, 3*step, 6)
kernAnimate.setValueAt(14, 3*step, 7)
kernAnimate.setValueAt(4, 3*step, 8)
kernAnimate.setValueAt(0, 3*step, 9)
kernAnimate.setValueAt(5, 3*step, 10)
kernAnimate.setValueAt(0, 3*step, 11)

kernAnimate.setValueAt(0, 4*step, 0)
kernAnimate.setValueAt(0, 4*step, 1)
kernAnimate.setValueAt(1, 4*step, 2)
kernAnimate.setValueAt(0, 4*step, 3)
kernAnimate.setValueAt(2, 4*step, 4)
kernAnimate.setValueAt(14, 4*step, 5)
kernAnimate.setValueAt(3, 4*step, 6)
kernAnimate.setValueAt(0, 4*step, 7)
kernAnimate.setValueAt(4, 4*step, 8)
kernAnimate.setValueAt(0, 4*step, 9)
kernAnimate.setValueAt(5, 4*step, 10)
kernAnimate.setValueAt(0, 4*step, 11)

kernAnimate.setValueAt(0, 5*step, 0)
kernAnimate.setValueAt(0, 5*step, 1)
kernAnimate.setValueAt(1, 5*step, 2)
kernAnimate.setValueAt(14, 5*step, 3)
kernAnimate.setValueAt(2, 5*step, 4)
kernAnimate.setValueAt(0, 5*step, 5)
kernAnimate.setValueAt(3, 5*step, 6)
kernAnimate.setValueAt(0, 5*step, 7)
kernAnimate.setValueAt(4, 5*step, 8)
kernAnimate.setValueAt(0, 5*step, 9)
kernAnimate.setValueAt(5, 5*step, 10)
kernAnimate.setValueAt(0, 5*step, 11)


transformNode = nuke.nodes.Transform(name="Transform1")
transformNode["translate"].setValue((-310, 778/2))
transformNode.setInput(0, textNode)


writeNode = nuke.nodes.Write(name="Write1", file="C:/Users/FPG/Desktop/1.mov", file_type="mov")
writeNode.setInput(0, transformNode)

nuke.execute(writeNode, 1, nuke.root().lastFrame(), 1)
# viewerNode = nuke.toNode("Viewer1")
# viewerNode.setInput(0, textNode)