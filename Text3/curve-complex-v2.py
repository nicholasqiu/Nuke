import nuke

xCurve = nuke.toNode("Transform1").knob("translate").animation(1)
xArr = []
yArr = []

firstFrame = 1
lastFrame = 90
sFirstValue = xCurve.evaluate(firstFrame)
sLastValue = xCurve.evaluate(lastFrame)
sDistance = sLastValue - sFirstValue

flag = False
if xCurve.evaluate(firstFrame + 1) > xCurve.evaluate(firstFrame + 0):
    flag = False
else:
    flag = True

equalFlag = False
if xCurve.evaluate(firstFrame + 1) == xCurve.evaluate(firstFrame + 0):
    equalFlag = True
else:
    equalFlag = False

xArr.append(1)
yArr.append(xCurve.evaluate(1))

for i in range(lastFrame - firstFrame):
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) == 0:
        if not equalFlag:
            xArr.append(firstFrame + i)
            yArr.append(xCurve.evaluate(firstFrame + i))
        equalFlag = True
    else:
        if equalFlag:
            if xCurve.evaluate(firstFrame + i + 1) > xCurve.evaluate(firstFrame + i):
                flag = False
            else:
                flag = True
        equalFlag = False

    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) > 0:
        if not flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = True
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) < 0:
        if flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = False

xArr.append(lastFrame)
yArr.append(xCurve.evaluate(lastFrame))

print xArr
print yArr

# tXArr = []
# tYArr = []
# tFirstValue = 700
# tLastValue = 1100
# tDistance = tLastValue - tFirstValue
#
# stDistance = tDistance - sDistance
#
# tYArr.append(tFirstValue)
# for j in range(len(yArr) - 1):
#    if yArr[j + 1] > yArr[j]:
#        tYArr.append(yArr[j + 1] + stDistance)
#    elif yArr[j + 1] < yArr[j]:
#        tYArr.append(tYArr[j] - (yArr[j] - yArr[j + 1] + stDistance))
#    else:
#        tYArr.append(tYArr[j])
#
# print tYArr


tXArr = []
tYArr = []
tFirstValue = 0
tLastValue = -800
tDistance = tLastValue - tFirstValue

stDistance = tDistance / sDistance * 1.0

# tYArr.append(tFirstValue)
for j in range(len(yArr)):
    #    if yArr[j + 1] > yArr[j]:
    #        tYArr.append((yArr[j + 1] - yArr[j]) * stDistance + yArr[j])
    #    elif yArr[j + 1] < yArr[j]:
    #        tYArr.append(tYArr[j] - (yArr[j] - yArr[j + 1] + stDistance))
    #    else:
    #        tYArr.append(tYArr[j])

    tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstValue)
    print (yArr[j] - yArr[0]) / sDistance * tDistance + tFirstValue

print tYArr

ratio = 0.83

tCurve = nuke.toNode("Transform2").knob("translate").animation(1)
tCurve.clear()
for k in range(len(tYArr) - 1):
    for i in range(int((xArr[k + 1] - xArr[k]) / ratio)):
        if yArr[k + 1] - yArr[k] == 0:
            tCurve.setKey(xArr[k] / ratio + i, tYArr[k])
        else:
            tCurve.setKey(xArr[k] / ratio + i,
                          (xCurve.evaluate(xArr[k] + i * ratio) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (
                                      tYArr[k + 1] - tYArr[k]) + tYArr[k])

tCurve.setKey(xArr[len(xArr) - 1] / ratio, tYArr[len(tYArr) - 1])












#nuke.thisNode().knob("knobChanged").setValue('''

tn = nuke.thisNode()
tk = nuke.thisKnob()

def getMinMaxValue(firstFrame, lastFrame, animationCurve):
    minValueX = firstFrame
    minValue = animationCurve.evaluate(firstFrame)
    maxValue = animationCurve.evaluate(firstFrame)
    maxValueX = firstFrame
    frameCount = int(lastFrame - firstFrame + 1)
    for i in range(frameCount):
        value = animationCurve.evaluate(i + firstFrame)
        if value > maxValue:
            maxValue = value
            maxValueX = i + firstFrame
        if value < minValue:
            minValue = value
            minValueX = i + firstFrame

    return [minValueX, minValue, maxValueX, maxValue]

def setSourceInfo(animationCurve):
    keysCount = animationCurve.size()
    firstFrame = animationCurve.keys()[0].x
    lastFrame = animationCurve.keys()[keysCount - 1].x
    tn.knob("sourceFirstFrameX").setValue(firstFrame)
    tn.knob("sourceLastFrameX").setValue(lastFrame)
    minMaxValue = getMinMaxValue(firstFrame, lastFrame, animationCurve)
    tn.knob("sourceFirstFrameY").setValue(animationCurve.evaluate(firstFrame))
    tn.knob("sourceLastFrameY").setValue(animationCurve.evaluate(lastFrame))
    #if (minMaxValue[0] == firstFrame and minMaxValue[2] == lastFrame) or (minMaxValue[0] == lastFrame and minMaxValue[2] == firstFrame):
        #tn.knob("sourceText").setValue("Have chosen")
    #else:
        #tn.knob("sourceText").setValue("无法复制此类曲线！！！")
    tn.knob("sourceText").setValue("Have chosen")

def getSourceInfo():
    sourceNodeName = tn.knob("sourceNodes").value()
    attributeMultipleName = tn.knob("attributeMultiple").value()
    attributeSingleName = tn.knob("attributeSingle").value()
    attributeSingleNames = tn.knob("attributeSingle").values()
    if sourceNodeName != "None" and attributeMultipleName != "None" and attributeSingleName != "None":
        indexArr = [i for i, name in enumerate(attributeSingleNames) if name == attributeSingleName]
        index = indexArr[0] - 1
        animationCurve = nuke.toNode(sourceNodeName).knob(attributeMultipleName).animations()[index]
        if len(animationCurve.keys()) > 1:
            setSourceInfo(animationCurve)
        else:
            tn.knob("sourceText").setValue("目标曲线的关键帧数量至少要有两个！！！")
    elif sourceNodeName != "None" and attributeMultipleName != "None" and len(attributeSingleNames) <= 1:
        animationCurve = nuke.toNode(sourceNodeName).knob(attributeMultipleName).animations()[0]
        if len(animationCurve.keys()) > 1:
            setSourceInfo(animationCurve)
        else:
            tn.knob("sourceText").setValue("目标曲线的关键帧数量至少要有两个！！！")
    else:
        tn.knob("sourceFirstFrameX").setValue(0)
        tn.knob("sourceLastFrameX").setValue(0)
        tn.knob("sourceFirstFrameY").setValue(0)
        tn.knob("sourceLastFrameY").setValue(0)
        tn.knob("sourceText").setValue("没有选择源曲线！！！")


attributeMultiple = ["None"]
attributeSingle = ["None"]

if tk.name() == "sourceNodes":
    nodeName = tk.value()
    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "" and nuke.toNode(nodeName)[knobName].isAnimated():
                attributeMultiple.append(knobName)

        tn.knob("attributeMultiple").setValues(attributeMultiple)
    else:
        tn.knob("attributeMultiple").setValues(["None"])

    tn.knob("attributeSingle").setValues(["None"])
    tn.knob("attributeMultiple").setValue(0)
    tn.knob("attributeSingle").setValue(0)

    getSourceInfo()

if tk.name() == "attributeMultiple":
    multipleAttrName = tk.value()
    if multipleAttrName != "None":
        nodeName = tn.knob("sourceNodes").value()
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.animations())):
                    attributeSingle.append(multipleAttrKnobs.animations()[i].knobAndFieldName().split(".")[1])
                tn.knob("attributeSingle").setValues(attributeSingle)
            else:
                tn.knob("attributeSingle").setValues(["None"])
        else:
            tn.knob("attributeSingle").setValues(["None"])
    else:
        tn.knob("attributeSingle").setValues(["None"])
    tn.knob("attributeSingle").setValue(0)

    getSourceInfo()

if tk.name() == "attributeSingle":
    getSourceInfo()





def chooiceTargetInfo():
    targetNodeName = tn.knob("targetNodes").value()
    targetAttributeMultipleName = tn.knob("targetAttributeMultiple").value()
    targetAttributeSingleName = tn.knob("targetAttributeSingle").value()
    targetAttributeSingleNames = tn.knob("targetAttributeSingle").values()
    targetFirstFrame = tn.knob("targetFirstFrameXUser").value()
    targetLastFrame = tn.knob("targetLastFrameXUser").value()

    showUserConfiguration = tn.knob("showUserConfiguration").value()
    targetState = tn.knob("targetState").value()
    if showUserConfiguration:
        if targetState == "show":
            tn.knob("targetText").setValue("Replicable")
        else:
            tn.knob("targetText").setValue("信息填写不完整！！！")
    else:
        if targetNodeName != "None" and targetAttributeMultipleName != "None" and targetAttributeSingleName != "None" and targetLastFrame > targetFirstFrame:
            tn.knob("targetText").setValue("Replicable")
        elif targetNodeName != "None" and targetAttributeMultipleName != "None" and len(targetAttributeSingleNames) <= 1 and targetLastFrame > targetFirstFrame:
            tn.knob("targetText").setValue("Replicable")
        else:
            tn.knob("targetText").setValue("信息填写不完整！！！")

def hideTargetInfo():
    tn.knob("targetFirstFrameX").setValue(0)
    tn.knob("targetFirstFrameY").setValue(0)
    tn.knob("targetLastFrameX").setValue(0)
    tn.knob("targetLastFrameY").setValue(0)
    tn.knob("targetFirstFrameX").setVisible(False)
    tn.knob("targetFirstFrameY").setVisible(False)
    tn.knob("targetLastFrameX").setVisible(False)
    tn.knob("targetLastFrameY").setVisible(False)

    tn.knob("targetState").setValue("hide")

def getTargetInfo():
    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()

    if (tNodeName != "None" and tAttrMultipleName != "None" and tAttrSingleName != "None") or (tNodeName != "None" and tAttrMultipleName != "None" and len(tAttrSingleNames) <= 1):
        tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
        tIndex = 0
        if len(tAttrSingleNames) > 1:
            tIndex = tIndexArr[0] - 1
        tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

        if tAnimation.isAnimated() and (tAnimation.animation(tIndex) != None) and len(tAnimation.animation(tIndex).keys()) > 1:
            tn.knob("targetFirstFrameX").setVisible(True)
            tn.knob("targetFirstFrameY").setVisible(True)
            tn.knob("targetLastFrameX").setVisible(True)
            tn.knob("targetLastFrameY").setVisible(True)
            tAnimationCurve = tAnimation.animation(tIndex)
            tkeysCount = len(tAnimationCurve.keys())
            tn.knob("targetFirstFrameX").setValue(tAnimationCurve.keys()[0].x)
            tn.knob("targetFirstFrameY").setValue(tAnimationCurve.keys()[0].y)
            tn.knob("targetLastFrameX").setValue(tAnimationCurve.keys()[tkeysCount - 1].x)
            tn.knob("targetLastFrameY").setValue(tAnimationCurve.keys()[tkeysCount - 1].y)

            tn.knob("targetState").setValue("show")
        else:
            hideTargetInfo()
    else:
        hideTargetInfo()


targetAttributeMultiple = ["None"]
targetAttributeSingle = ["None"]

if tk.name() == "targetNodes":
    nodeName = tk.value()
    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "":
                targetAttributeMultiple.append(knobName)
        tn.knob("targetAttributeMultiple").setValues(targetAttributeMultiple)
    else:
        tn.knob("targetAttributeMultiple").setValues(["None"])

    tn.knob("targetAttributeSingle").setValues(["None"])
    tn.knob("targetAttributeMultiple").setValue(0)
    tn.knob("targetAttributeSingle").setValue(0)

    getTargetInfo()

if tk.name() == "targetAttributeMultiple":
    multipleAttrName = tk.value()
    if multipleAttrName != "None":
        nodeName = tn.knob("targetNodes").value()
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.vect())):
                    targetAttributeSingle.append(multipleAttrKnobs.names(i))
                tn.knob("targetAttributeSingle").setValues(targetAttributeSingle)
            else:
                tn.knob("targetAttributeSingle").setValues(["None"])
        else:
            tn.knob("targetAttributeSingle").setValues(["None"])
    else:
        tn.knob("targetAttributeSingle").setValues(["None"])
    tn.knob("targetAttributeSingle").setValue(0)

    getTargetInfo()

if tk.name() == "targetAttributeSingle":
    getTargetInfo()

if tk.name() == "showUserConfiguration":
    if tn.knob("showUserConfiguration").value():
        tn.knob("targetFirstFrameXUser").setVisible(False)
        tn.knob("targetFirstFrameYUser").setVisible(False)
        tn.knob("targetLastFrameXUser").setVisible(False)
        tn.knob("targetLastFrameYUser").setVisible(False)
        tn.knob("targetFrameText").setVisible(False)
    else:
        tn.knob("targetFirstFrameXUser").setVisible(True)
        tn.knob("targetFirstFrameYUser").setVisible(True)
        tn.knob("targetLastFrameXUser").setVisible(True)
        tn.knob("targetLastFrameYUser").setVisible(True)
        tn.knob("targetFrameText").setVisible(True)


chooiceTargetInfo()

if tn.knob("sourceText").value() == "Have chosen" and tn.knob("targetText").value() == "Replicable":
    tn.knob("copyCurve").setEnabled(True)
    tn.knob("reverseCurve").setEnabled(True)
else:
    tn.knob("copyCurve").setEnabled(False)
    tn.knob("reverseCurve").setEnabled(False)

#''')





if tn.knob("sourceText").value() == "Have chosen" and tn.knob("targetText").value() == "Replicable":
    sNodeName = tn.knob("sourceNodes").value()
    sAttrMultipleName = tn.knob("attributeMultiple").value()
    sAttrSingleName = tn.knob("attributeSingle").value()
    sAttrSingleNames = tn.knob("attributeSingle").values()
    sFirstFrame = tn.knob("sourceFirstFrameX").value()
    sLastFrame = tn.knob("sourceLastFrameX").value()
    sFirstFrameY = tn.knob("sourceFirstFrameY").value()
    sLastFrameY = tn.knob("sourceLastFrameY").value()
    sDistance = sLastFrameY - sFirstFrameY
    sFrameCount = sLastFrame - sFirstFrame + 1

    sIndexArr = [i for i, name in enumerate(sAttrSingleNames) if name == sAttrSingleName]
    sIndex = 0
    if len(sAttrSingleNames) > 1:
        sIndex = sIndexArr[0] - 1
    sAnimationCurve = nuke.toNode(sNodeName).knob(sAttrMultipleName).animations()[sIndex]

    xArr = []
    yArr = []

    flag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) > sAnimationCurve.evaluate(sFirstFrame + 0):
        flag = False
    else:
        flag = True

    equalFlag = False
    if sAnimationCurve.evaluate(sFirstFrame + 1) == sAnimationCurve.evaluate(sFirstFrame + 0):
        equalFlag = True
    else:
        equalFlag = False

    xArr.append(sFirstFrame)
    yArr.append(sAnimationCurve.evaluate(sFirstFrame))

    for i in range(int(sLastFrame - sFirstFrame)):
        if sAnimationCurve.evaluate(sFirstFrame + i + 1) - sAnimationCurve.evaluate(sFirstFrame + i) == 0:
            if not equalFlag:
                xArr.append(sFirstFrame + i)
                yArr.append(sAnimationCurve.evaluate(sFirstFrame + i))
            equalFlag = True
        else:
            if equalFlag:
                if sAnimationCurve.evaluate(sFirstFrame + i + 1) > sAnimationCurve.evaluate(sFirstFrame + i):
                    flag = False
                else:
                    flag = True
            equalFlag = False

        if sAnimationCurve.evaluate(sFirstFrame + i + 1) - sAnimationCurve.evaluate(sFirstFrame + i) > 0:
            if not flag:
                if i != 0:
                    xArr.append(sFirstFrame + i)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i))
            flag = True
        if sAnimationCurve.evaluate(sFirstFrame + i + 1) - sAnimationCurve.evaluate(sFirstFrame + i) < 0:
            if flag:
                if i != 0:
                    xArr.append(sFirstFrame + i)
                    yArr.append(sAnimationCurve.evaluate(sFirstFrame + i))
            flag = False

    xArr.append(sLastFrame)
    yArr.append(sAnimationCurve.evaluate(sLastFrame))


    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()
    tFirstFrame = tn.knob("targetFirstFrameX").value()
    tLastFrame = tn.knob("targetLastFrameX").value()
    tFirstFrameY = tn.knob("targetFirstFrameY").value()
    tLastFrameY = tn.knob("targetLastFrameY").value()
    if not tn.knob("showUserConfiguration").value():
        tFirstFrame = tn.knob("targetFirstFrameXUser").value()
        tLastFrame = tn.knob("targetLastFrameXUser").value()
        tFirstFrameY = tn.knob("targetFirstFrameYUser").value()
        tLastFrameY = tn.knob("targetLastFrameYUser").value()
    tFrameCount = tLastFrame - tFirstFrame + 1


    tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
    tIndex = 0
    if len(tAttrSingleNames) > 1:
        tIndex = tIndexArr[0] - 1
    tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)
    tAnimation.setAnimated(tIndex)

    tAnimationCurve = nuke.toNode(tNodeName).knob(tAttrMultipleName).animations()[tIndex]

    frameMultiple = sFrameCount / tFrameCount * 1.0

    tYArr = []
    tDistance = tLastFrameY - tFirstFrameY
    stDistance = tDistance / sDistance * 1.0

    for j in range(len(yArr)):
        tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstFrameY)

    if tAnimation.isAnimated():
        tAnimationCurve.clear()


    for k in range(len(tYArr) - 1):
        for i in range(int((xArr[k + 1] - xArr[k]) / frameMultiple)):
            if yArr[k + 1] - yArr[k] == 0:
                tAnimationCurve.setKey(xArr[k] / frameMultiple + i, tYArr[k])
            else:
                tAnimationCurve.setKey(xArr[k] / frameMultiple + i, (sAnimationCurve.evaluate(xArr[k] + i * frameMultiple) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k])
    tAnimationCurve.setKey(tLastFrame, tLastFrameY)

    dir = -1
    if (sLastFrameY > sFirstFrameY and tLastFrameY > tFirstFrameY) or (sLastFrameY < sFirstFrameY and tLastFrameY < tFirstFrameY):
        dir = 1
    sKeysLen = len(sAnimationCurve.keys())
    tKeysLen = len(tAnimationCurve.keys())
    tAnimationCurve.keys()[tKeysLen - 1].la = sAnimationCurve.keys()[sKeysLen - 1].la * dir
    tAnimationCurve.keys()[tKeysLen - 1].lslope = sAnimationCurve.keys()[sKeysLen - 1].lslope * dir
    tAnimationCurve.keys()[tKeysLen - 1].ra = sAnimationCurve.keys()[sKeysLen - 1].ra * dir
    tAnimationCurve.keys()[tKeysLen - 1].rslope = sAnimationCurve.keys()[sKeysLen - 1].rslope * dir















xCurve = nuke.toNode("Transform1").knob("translate").animation(1)
xArr = []
yArr = []

firstFrame = 0
lastFrame = 60
sFirstValue = xCurve.evaluate(firstFrame)
sLastValue = xCurve.evaluate(lastFrame)
sDistance = sLastValue - sFirstValue

flag = False
if xCurve.evaluate(firstFrame + 1) > xCurve.evaluate(firstFrame + 0):
    flag = False
else:
    flag = True

equalFlag = False
if xCurve.evaluate(firstFrame + 1) == xCurve.evaluate(firstFrame + 0):
    equalFlag = True
else:
    equalFlag = False


xArr.append(0)
yArr.append(xCurve.evaluate(0))

for i in range(lastFrame - firstFrame):
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) == 0:
        if not equalFlag:
            xArr.append(firstFrame + i)
            yArr.append(xCurve.evaluate(firstFrame + i))
        equalFlag = True
    else:
        if equalFlag:
            if xCurve.evaluate(firstFrame + i+ 1) > xCurve.evaluate(firstFrame + i):
                flag = False
            else:
                flag = True
        equalFlag = False

    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) > 0:
        if not flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = True
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) < 0:
        if flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = False

xArr.append(lastFrame)
yArr.append(xCurve.evaluate(lastFrame))

print xArr
print yArr

tXArr = []
tYArr = []
tFirstValue = -100
tLastValue =400
tDistance = tLastValue - tFirstValue

stDistance = tDistance / sDistance * 1.0

for j in range(len(yArr)):
    tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstValue)

print tYArr

tCurve = nuke.toNode("Transform2").knob("translate").animation(1)
tCurve.clear()
for k in range(len(tYArr) - 1):
    tCurve.setKey(xArr[k], tYArr[k])
    for i in range(int(xArr[k + 1] - xArr[k])):
        for key in xCurve.keys():
            if key.x == xArr[k] + i:
                if yArr[k + 1] - yArr[k] == 0:
                    tCurve.setKey(xArr[k] + i, tYArr[k])
                else:
                    tCurve.setKey(xArr[k] + i, (xCurve.evaluate(xArr[k] + i) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k])

tCurve.setKey(xArr[len(xArr) - 1], tYArr[len(tYArr) - 1])


for k in range(len(xCurve.keys()) - 1):
    for i in range(len(tCurve.keys()) - 1):
#        print xCurve.keys()[k].lslope
        if xCurve.keys()[k].x == tCurve.keys()[i].x:
            tCurve.keys()[i].la = xCurve.keys()[k].la
            tCurve.keys()[i].lslope = xCurve.keys()[k].lslope
            tCurve.keys()[i].ra = xCurve.keys()[k].ra
            tCurve.keys()[i].rslope = xCurve.keys()[k].rslope
#for i in range(len(xCurve.keys())):
#    tCurve.keys()[i].la = xCurve.keys()[i].la
#    tCurve.keys()[i].lslope = xCurve.keys()[i].lslope
#    tCurve.keys()[i].ra = xCurve.keys()[i].ra
#    tCurve.keys()[i].rslope = xCurve.keys()[i].rslope


#for i in range(len(xCurve.keys())):
#    tCurve.setKey(xCurve.keys()[i].x, xCurve.keys()[i].y)
#for i in range(len(xCurve.keys())):
#    tCurve.keys()[i].la = xCurve.keys()[i].la
#    tCurve.keys()[i].lslope = xCurve.keys()[i].lslope
#    tCurve.keys()[i].ra = xCurve.keys()[i].ra
#    tCurve.keys()[i].rslope = xCurve.keys()[i].rslope











xCurve = nuke.toNode("Transform1").knob("translate").animation(1)
xArr = []
yArr = []

firstFrame = 0
lastFrame = 60
sFirstValue = xCurve.evaluate(firstFrame)
sLastValue = xCurve.evaluate(lastFrame)
sDistance = sLastValue - sFirstValue

flag = False
if xCurve.evaluate(firstFrame + 1) > xCurve.evaluate(firstFrame + 0):
    flag = False
else:
    flag = True

equalFlag = False
if xCurve.evaluate(firstFrame + 1) == xCurve.evaluate(firstFrame + 0):
    equalFlag = True
else:
    equalFlag = False


xArr.append(0)
yArr.append(xCurve.evaluate(0))

for i in range(lastFrame - firstFrame):
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) == 0:
        if not equalFlag:
            xArr.append(firstFrame + i)
            yArr.append(xCurve.evaluate(firstFrame + i))
        equalFlag = True
    else:
        if equalFlag:
            if xCurve.evaluate(firstFrame + i+ 1) > xCurve.evaluate(firstFrame + i):
                flag = False
            else:
                flag = True
        equalFlag = False

    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) > 0:
        if not flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = True
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) < 0:
        if flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = False

xArr.append(lastFrame)
yArr.append(xCurve.evaluate(lastFrame))

print xArr
print yArr

tXArr = []
tYArr = []
tFirstValue = 20
tLastValue = 520
tDistance = tLastValue - tFirstValue

stDistance = tDistance / sDistance * 1.0

for j in range(len(yArr)):
    tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstValue)

print tYArr

tCurve = nuke.toNode("Transform2").knob("scale").animation(0)
tCurve.clear()
for k in range(len(tYArr) - 1):
    tCurve.setKey(xArr[k], tYArr[k])
    for i in range(int(xArr[k + 1] - xArr[k])):
        for key in xCurve.keys():
            if key.x == xArr[k] + i:
                if yArr[k + 1] - yArr[k] == 0:
                    tCurve.setKey(xArr[k] + i, tYArr[k])
                else:
                    tCurve.setKey(xArr[k] + i, (xCurve.evaluate(xArr[k] + i) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k])

tCurve.setKey(xArr[len(xArr) - 1], tYArr[len(tYArr) - 1])


dir = 1
sKeysLen = len(xCurve.keys())
tKeysLen = len(tCurve.keys())
tCurve.keys()[tKeysLen - 1].la = xCurve.keys()[sKeysLen - 1].la * dir
tCurve.keys()[tKeysLen - 1].lslope = xCurve.keys()[sKeysLen - 1].lslope * dir
tCurve.keys()[tKeysLen - 1].ra = xCurve.keys()[sKeysLen - 1].ra * dir
tCurve.keys()[tKeysLen - 1].rslope = xCurve.keys()[sKeysLen - 1].rslope * dir
#for i in range(len(xCurve.keys())):
#    tCurve.keys()[i].la = xCurve.keys()[i].la
#    tCurve.keys()[i].lslope = xCurve.keys()[i].lslope
#    tCurve.keys()[i].ra = xCurve.keys()[i].ra
#    tCurve.keys()[i].rslope = xCurve.keys()[i].rslope


#for i in range(len(xCurve.keys())):
#    tCurve.setKey(xCurve.keys()[i].x, xCurve.keys()[i].y)
#for i in range(len(xCurve.keys())):
#    tCurve.keys()[i].la = xCurve.keys()[i].la
#    tCurve.keys()[i].lslope = xCurve.keys()[i].lslope
#    tCurve.keys()[i].ra = xCurve.keys()[i].ra
#    tCurve.keys()[i].rslope = xCurve.keys()[i].rslope









xCurve = nuke.toNode("Transform1").knob("translate").animation(1)
xArr = []
yArr = []

firstFrame = 0
lastFrame = 60
sFirstValue = xCurve.evaluate(firstFrame)
sLastValue = xCurve.evaluate(lastFrame)
sDistance = sLastValue - sFirstValue

flag = False
if xCurve.evaluate(firstFrame + 1) > xCurve.evaluate(firstFrame + 0):
    flag = False
else:
    flag = True

equalFlag = False
if xCurve.evaluate(firstFrame + 1) == xCurve.evaluate(firstFrame + 0):
    equalFlag = True
else:
    equalFlag = False


xArr.append(0)
yArr.append(xCurve.evaluate(0))

for i in range(lastFrame - firstFrame):
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) == 0:
        if not equalFlag:
            xArr.append(firstFrame + i)
            yArr.append(xCurve.evaluate(firstFrame + i))
        equalFlag = True
    else:
        if equalFlag:
            if xCurve.evaluate(firstFrame + i+ 1) > xCurve.evaluate(firstFrame + i):
                flag = False
            else:
                flag = True
        equalFlag = False

    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) > 0:
        if not flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = True
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) < 0:
        if flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = False

xArr.append(lastFrame)
yArr.append(xCurve.evaluate(lastFrame))

print xArr
print yArr

tXArr = []
tYArr = []
tFirstValue = 0
tLastValue = 500
tDistance = tLastValue - tFirstValue

stDistance = tDistance / sDistance * 1.0

for j in range(len(yArr)):
    tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstValue)

print tYArr

tCurve = nuke.toNode("Transform2").knob("translate").animation(0)
tCurve.clear()
for k in range(len(tYArr) - 1):
    tCurve.setKey(xArr[k], tYArr[k])
#    for i in range(int(xArr[k + 1] - xArr[k])):
#        if yArr[k + 1] - yArr[k] == 0:
#            tCurve.setKey(xArr[k] + i, tYArr[k])
#        else:
#            tCurve.setKey(xArr[k] + i, (xCurve.evaluate(xArr[k] + i) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k])

tCurve.setKey(xArr[len(xArr) - 1], tYArr[len(tYArr) - 1])


dir = 1
sKeysLen = len(xCurve.keys())
tKeysLen = len(tCurve.keys())
tCurve.keys()[tKeysLen - 1].la = xCurve.keys()[sKeysLen - 1].la * dir
tCurve.keys()[tKeysLen - 1].lslope = xCurve.keys()[sKeysLen - 1].lslope * dir
tCurve.keys()[tKeysLen - 1].ra = xCurve.keys()[sKeysLen - 1].ra * dir
tCurve.keys()[tKeysLen - 1].rslope = xCurve.keys()[sKeysLen - 1].rslope * dir












xCurve = nuke.toNode("Transform1").knob("translate").animation(1)
xArr = []
yArr = []

firstFrame = 0
lastFrame = 60
sFirstValue = xCurve.evaluate(firstFrame)
sLastValue = xCurve.evaluate(lastFrame)
sDistance = sLastValue - sFirstValue

flag = False
if xCurve.evaluate(firstFrame + 1) > xCurve.evaluate(firstFrame + 0):
    flag = False
else:
    flag = True

equalFlag = False
if xCurve.evaluate(firstFrame + 1) == xCurve.evaluate(firstFrame + 0):
    equalFlag = True
else:
    equalFlag = False


xArr.append(0)
yArr.append(xCurve.evaluate(0))

for i in range(lastFrame - firstFrame):
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) == 0:
        if not equalFlag:
            xArr.append(firstFrame + i)
            yArr.append(xCurve.evaluate(firstFrame + i))
        equalFlag = True
    else:
        if equalFlag:
            if xCurve.evaluate(firstFrame + i+ 1) > xCurve.evaluate(firstFrame + i):
                flag = False
            else:
                flag = True
        equalFlag = False

    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) > 0:
        if not flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = True
    if xCurve.evaluate(firstFrame + i + 1) - xCurve.evaluate(firstFrame + i) < 0:
        if flag:
            if i != 0:
                xArr.append(firstFrame + i)
                yArr.append(xCurve.evaluate(firstFrame + i))
        flag = False

xArr.append(lastFrame)
yArr.append(xCurve.evaluate(lastFrame))

print xArr
print yArr

tXArr = []
tYArr = []
tFirstValue = 100
tLastValue = 600
tDistance = tLastValue - tFirstValue

stDistance = tDistance / sDistance * 1.0

for j in range(len(yArr)):
    tYArr.append((yArr[j] - yArr[0]) / sDistance * tDistance + tFirstValue)

print tYArr

tCurve = nuke.toNode("Transform2").knob("rotate").animation(0)
tCurve.clear()
for k in range(len(tYArr) - 1):
    for i in range(int(xArr[k + 1] - xArr[k])):
        if yArr[k + 1] - yArr[k] == 0:
            tCurve.setKey(xArr[k] + i, tYArr[k])
        else:
            tCurve.setKey(xArr[k] + i, (xCurve.evaluate(xArr[k] + i) - yArr[k]) / (yArr[k + 1] - yArr[k]) * (tYArr[k + 1] - tYArr[k]) + tYArr[k])

tCurve.setKey(xArr[len(xArr) - 1], tYArr[len(tYArr) - 1])


dir = 1
sKeysLen = len(xCurve.keys())
tKeysLen = len(tCurve.keys())
tCurve.keys()[tKeysLen - 1].la = xCurve.keys()[sKeysLen - 1].la * dir
tCurve.keys()[tKeysLen - 1].lslope = xCurve.keys()[sKeysLen - 1].lslope * dir
tCurve.keys()[tKeysLen - 1].ra = xCurve.keys()[sKeysLen - 1].ra * dir
tCurve.keys()[tKeysLen - 1].rslope = xCurve.keys()[sKeysLen - 1].rslope * dir






