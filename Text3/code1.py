import nuke
#nuke.thisNode().knob("knobChanged").setValue('''

tn = nuke.thisNode()
tk = nuke.thisKnob()

def getMinMaxValue(firstFrame, lastFrame, animationCurve):
    minValueX = firstFrame
    minValue = animationCurve.evaluate(firstFrame)
    maxValue = animationCurve.evaluate(firstFrame)
    maxValueX = firstFrame
    frameCount = int(lastFrame - firstFrame + 1)
    for i in range(frameCount):
        value = animationCurve.evaluate(i + firstFrame)
        if value > maxValue:
            maxValue = value
            maxValueX = i + firstFrame
        if value < minValue:
            minValue = value
            minValueX = i + firstFrame

    return [minValueX, minValue, maxValueX, maxValue]

def setSourceInfo(animationCurve):
    keysCount = animationCurve.size()
    firstFrame = animationCurve.keys()[0].x
    lastFrame = animationCurve.keys()[keysCount - 1].x
    tn.knob("sourceFirstFrameX").setValue(firstFrame)
    tn.knob("sourceLastFrameX").setValue(lastFrame)
    minMaxValue = getMinMaxValue(firstFrame, lastFrame, animationCurve)
    tn.knob("sourceFirstFrameY").setValue(animationCurve.evaluate(firstFrame))
    tn.knob("sourceLastFrameY").setValue(animationCurve.evaluate(lastFrame))
    if (minMaxValue[0] == firstFrame and minMaxValue[2] == lastFrame) or (minMaxValue[0] == lastFrame and minMaxValue[2] == firstFrame):
        tn.knob("sourceText").setValue("Have chosen")
    else:
        tn.knob("sourceText").setValue("无法复制此类曲线！！！")

def getSourceInfo():
    sourceNodeName = tn.knob("sourceNodes").value()
    attributeMultipleName = tn.knob("attributeMultiple").value()
    attributeSingleName = tn.knob("attributeSingle").value()
    attributeSingleNames = tn.knob("attributeSingle").values()
    if sourceNodeName != "None" and attributeMultipleName != "None" and attributeSingleName != "None":
        indexArr = [i for i, name in enumerate(attributeSingleNames) if name == attributeSingleName]
        index = indexArr[0] - 1
        animationCurve = nuke.toNode(sourceNodeName).knob(attributeMultipleName).animations()[index]
        setSourceInfo(animationCurve)
    elif sourceNodeName != "None" and attributeMultipleName != "None" and len(attributeSingleNames) <= 1:
        animationCurve = nuke.toNode(sourceNodeName).knob(attributeMultipleName).animations()[0]
        setSourceInfo(animationCurve)
    else:
        tn.knob("sourceFirstFrameX").setValue(0)
        tn.knob("sourceLastFrameX").setValue(0)
        tn.knob("sourceFirstFrameY").setValue(0)
        tn.knob("sourceLastFrameY").setValue(0)
        tn.knob("sourceText").setValue("没有选择源曲线！！！")


attributeMultiple = ["None"]
attributeSingle = ["None"]

if tk.name() == "sourceNodes":
    nodeName = tk.value()
    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "" and nuke.toNode(nodeName)[knobName].isAnimated():
                attributeMultiple.append(knobName)

        tn.knob("attributeMultiple").setValues(attributeMultiple)
    else:
        tn.knob("attributeMultiple").setValues(["None"])

    tn.knob("attributeSingle").setValues(["None"])
    tn.knob("attributeMultiple").setValue(0)
    tn.knob("attributeSingle").setValue(0)

    getSourceInfo()

if tk.name() == "attributeMultiple":
    multipleAttrName = tk.value()
    if multipleAttrName != "None":
        nodeName = tn.knob("sourceNodes").value()
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.animations())):
                    attributeSingle.append(multipleAttrKnobs.animations()[i].knobAndFieldName().split(".")[1])
                tn.knob("attributeSingle").setValues(attributeSingle)
            else:
                tn.knob("attributeSingle").setValues(["None"])
        else:
            tn.knob("attributeSingle").setValues(["None"])
    else:
        tn.knob("attributeSingle").setValues(["None"])
    tn.knob("attributeSingle").setValue(0)

    getSourceInfo()

if tk.name() == "attributeSingle":
    getSourceInfo()





def chooiceTargetInfo():
    targetNodeName = tn.knob("targetNodes").value()
    targetAttributeMultipleName = tn.knob("targetAttributeMultiple").value()
    targetAttributeSingleName = tn.knob("targetAttributeSingle").value()
    targetAttributeSingleNames = tn.knob("targetAttributeSingle").values()
    targetFirstFrame = tn.knob("targetFirstFrameXUser").value()
    targetLastFrame = tn.knob("targetLastFrameXUser").value()

    showUserConfiguration = tn.knob("showUserConfiguration").value()
    targetState = tn.knob("targetState").value()
    if showUserConfiguration:
        if targetState == "show":
            tn.knob("targetText").setValue("Replicable")
        else:
            tn.knob("targetText").setValue("信息填写不完整！！！")
    else:
        if targetNodeName != "None" and targetAttributeMultipleName != "None" and targetAttributeSingleName != "None" and targetLastFrame > targetFirstFrame:
            tn.knob("targetText").setValue("Replicable")
        elif targetNodeName != "None" and targetAttributeMultipleName != "None" and len(targetAttributeSingleNames) <= 1 and targetLastFrame > targetFirstFrame:
            tn.knob("targetText").setValue("Replicable")
        else:
            tn.knob("targetText").setValue("信息填写不完整！！！")

def hideTargetInfo():
    tn.knob("targetFirstFrameX").setValue(0)
    tn.knob("targetFirstFrameY").setValue(0)
    tn.knob("targetLastFrameX").setValue(0)
    tn.knob("targetLastFrameY").setValue(0)
    tn.knob("targetFirstFrameX").setVisible(False)
    tn.knob("targetFirstFrameY").setVisible(False)
    tn.knob("targetLastFrameX").setVisible(False)
    tn.knob("targetLastFrameY").setVisible(False)

    tn.knob("targetState").setValue("hide")

def getTargetInfo():
    tNodeName = tn.knob("targetNodes").value()
    tAttrMultipleName = tn.knob("targetAttributeMultiple").value()
    tAttrSingleName = tn.knob("targetAttributeSingle").value()
    tAttrSingleNames = tn.knob("targetAttributeSingle").values()

    if (tNodeName != "None" and tAttrMultipleName != "None" and tAttrSingleName != "None") or (tNodeName != "None" and tAttrMultipleName != "None" and len(tAttrSingleNames) <= 1):
        tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
        tIndex = 0
        if len(tAttrSingleNames) > 1:
            tIndex = tIndexArr[0] - 1
        tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

        if tAnimation.isAnimated() and (tAnimation.animation(tIndex) != None) and len(tAnimation.animation(tIndex).keys()) > 1:
            tn.knob("targetFirstFrameX").setVisible(True)
            tn.knob("targetFirstFrameY").setVisible(True)
            tn.knob("targetLastFrameX").setVisible(True)
            tn.knob("targetLastFrameY").setVisible(True)
            tAnimationCurve = tAnimation.animation(tIndex)
            tkeysCount = len(tAnimationCurve.keys())
            tn.knob("targetFirstFrameX").setValue(tAnimationCurve.keys()[0].x)
            tn.knob("targetFirstFrameY").setValue(tAnimationCurve.keys()[0].y)
            tn.knob("targetLastFrameX").setValue(tAnimationCurve.keys()[tkeysCount - 1].x)
            tn.knob("targetLastFrameY").setValue(tAnimationCurve.keys()[tkeysCount - 1].y)

            tn.knob("targetState").setValue("show")
        else:
            hideTargetInfo()
    else:
        hideTargetInfo()


targetAttributeMultiple = ["None"]
targetAttributeSingle = ["None"]

if tk.name() == "targetNodes":
    nodeName = tk.value()
    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "":
                targetAttributeMultiple.append(knobName)
        tn.knob("targetAttributeMultiple").setValues(targetAttributeMultiple)
    else:
        tn.knob("targetAttributeMultiple").setValues(["None"])

    tn.knob("targetAttributeSingle").setValues(["None"])
    tn.knob("targetAttributeMultiple").setValue(0)
    tn.knob("targetAttributeSingle").setValue(0)

    getTargetInfo()

if tk.name() == "targetAttributeMultiple":
    multipleAttrName = tk.value()
    if multipleAttrName != "None":
        nodeName = tn.knob("targetNodes").value()
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.vect())):
                    targetAttributeSingle.append(multipleAttrKnobs.names(i))
                tn.knob("targetAttributeSingle").setValues(targetAttributeSingle)
            else:
                tn.knob("targetAttributeSingle").setValues(["None"])
        else:
            tn.knob("targetAttributeSingle").setValues(["None"])
    else:
        tn.knob("targetAttributeSingle").setValues(["None"])
    tn.knob("targetAttributeSingle").setValue(0)

    getTargetInfo()

if tk.name() == "targetAttributeSingle":
    getTargetInfo()

if tk.name() == "showUserConfiguration":
    if tn.knob("showUserConfiguration").value():
        tn.knob("targetFirstFrameXUser").setVisible(False)
        tn.knob("targetFirstFrameYUser").setVisible(False)
        tn.knob("targetLastFrameXUser").setVisible(False)
        tn.knob("targetLastFrameYUser").setVisible(False)
        tn.knob("targetFrameText").setVisible(False)
    else:
        tn.knob("targetFirstFrameXUser").setVisible(True)
        tn.knob("targetFirstFrameYUser").setVisible(True)
        tn.knob("targetLastFrameXUser").setVisible(True)
        tn.knob("targetLastFrameYUser").setVisible(True)
        tn.knob("targetFrameText").setVisible(True)


chooiceTargetInfo()

if tn.knob("sourceText").value() == "Have chosen" and tn.knob("targetText").value() == "Replicable":
    tn.knob("copyCurve").setEnabled(True)
    tn.knob("reverseCurve").setEnabled(True)
else:
    tn.knob("copyCurve").setEnabled(False)
    tn.knob("reverseCurve").setEnabled(False)

#''')
