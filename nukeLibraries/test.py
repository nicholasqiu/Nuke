# PT.y = powf(1 - t, 3) * P1.y + 3.0f * powf(1 - t, 2) * t * P2.y + 3.0f * (1 - t) * t * t * P3.y + t * t * t * P4.y;


import math

py1 = 0
py4 = 1
py2 = 0.885
py3 = 1.275

for i in range(11):
    t = i/10.0
    print math.pow(1-t, 3)*py1 + 3.0*math.pow(1-t, 2)*t*py2+3.0*(1-t)*t*t*py3+t*t*t*py4