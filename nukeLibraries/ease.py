import math

def getYHTriangle(num):
    a = [1, 1]
    b = []

    if num <= 1:
        return [1, 1]

    for i in range(num - 1):
        b.append(1)
        aLength = len(a)
        for j in range(aLength):
            if j < aLength - 1:
                b.append(a[j] + a[j + 1])
        b.append(1)

        a = b[:]
        b = []

    return a


def getEquation(rank, pointArr, t):
    yhTriangleArr = getYHTriangle(rank)

    equation = 0
    for i in range(len(pointArr)):
        equation += yhTriangleArr[i] * math.pow(1 - t, rank - i) * math.pow(t, i) * pointArr[i]

    return equation


def linear(i):
    return getEquation(1, [0, 1], i / 1.0)

def easeInSine(i):
    return getEquation(3, [0, 0, 0.715, 1], i / 1.0)

def easeOutSine(i):
    return getEquation(3, [0, 0.575, 1, 1], i / 1.0)

def easeInOutSine(i):
    return getEquation(3, [0, 0.05, 0.95, 1], i / 1.0)

def easeInQuad(i):
    return getEquation(3, [0, 0.085, 0.53, 1], i / 1.0)

def easeOutQuad(i):
    return getEquation(3, [0, 0.46, 0.94, 1], i / 1.0)

def easeInOutQuad(i):
    return getEquation(3, [0, 0.03, 0.955, 1], i / 1.0)

def easeInCubic(i):
    return getEquation(3, [0, 0.055, 0.19, 1], i / 1.0)

def easeOutCubic(i):
    return getEquation(3, [0, 0.61, 1, 1], i / 1.0)

def easeInOutCubic(i):
    return getEquation(3, [0, 0.045, 1, 1], i / 1.0)

def easeInQuart(i):
    return getEquation(3, [0, 0.03, 0.22, 1], i / 1.0)

def easeOutQuart(i):
    return getEquation(3, [0, 0.84, 1, 1], i / 1.0)

def easeInOutQuart(i):
    return getEquation(3, [0, 0, 1, 1], i / 1.0)

def easeInQuint(i):
    return getEquation(3, [0, 0.05, 0.06, 1], i / 1.0)

def easeOutQuint(i):
    return getEquation(3, [0, 1, 1, 1], i / 1.0)

def easeInOutQuint(i):
    return getEquation(3, [0, 0, 1, 1], i / 1.0)

def easeInExpo(i):
    return getEquation(3, [0, 0.05, 0.035, 1], i / 1.0)

def easeOutExpo(i):
    return getEquation(3, [0, 1, 1, 1], i / 1.0)

def easeInOutExpo(i):
    return getEquation(3, [0, 0, 1, 1], i / 1.0)

def easeInCirc(i):
    return getEquation(3, [0, 0.04, 0.335, 1], i / 1.0)

def easeOutCirc(i):
    return getEquation(3, [0, 0.82, 1, 1], i / 1.0)

def easeInOutCirc(i):
    return getEquation(3, [0, 0.135, 0.86, 1], i / 1.0)

def easeInBack(i):
    return getEquation(3, [0, -0.28, 0.045, 1], i / 1.0)

def easeOutBack(i):
    return getEquation(3, [0, 0.885, 1.275, 1], i / 1.0)

def easeInOutBack(i):
    return getEquation(3, [0, -0.55, 1.55, 1], i / 1.0)



for i in range(11):
    print easeInOutQuart(i/10.0)


























# def getYValue(i):
#     py1 = 0
#     py4 = 1
#     py2 = 0.885
#     py3 = 1.275
#
#     t = i/1.0
#     return (math.pow(1-t, 3)*py1 + 3.0*math.pow(1-t, 2)*t*py2+3.0*(1-t)*t*t*py3+t*t*t*py4)



#
# transformNode = nuke.toNode("Transform1")
#
# kTransform = transformNode["translate"]
#
# print kTransform.animations()
#
# keyX = kTransform.animation(0).keys()
# keyY = kTransform.animation(1).keys()
#
# print keyX
# print keyY
#
#
# for i in range(len(keyX)):
#     keyY[i].la = keyX[i].la
#     keyY[i].lslope = keyX[i].lslope
#     keyY[i].ra = keyX[i].ra
#     keyY[i].rslope = keyX[i].rslope
#    print keyX[i].la
#    print keyX[i].lslope
#    print keyX[i].ra
#    print keyX[i].rslope
#
#    print "======="
#
#    print keyY[i].la
#    print keyY[i].lslope
#    print keyY[i].ra
#    print keyY[i].rslope


# import math
#
# transformNode = nuke.toNode("Transform1")
#
# kTransform = transformNode["translate"]
#
# xMaxValue = 0
# yMaxValue = 0
#
# endFrame = nuke.root()["last_frame"].getValue()
#
# for i in range(int(endFrame)+1):
#     xValue = kTransform.animation(0).evaluate(i)
#     if xValue  > xMaxValue:
#         xMaxValue = xValue
#
#     yValue = kTransform.animation(1).evaluate(i)
#     if yValue  < yMaxValue:
#         yMaxValue = yValue
#
#
# for i in range(int(endFrame)+1):
#     kTransform.animation(1).setKey(i, kTransform.animation(0).evaluate(i)/(xMaxValue/yMaxValue))

