import time
import ease
import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from bs4 import BeautifulSoup
import os
os.environ["NUKE_INTERACTIVE"] = "1"

import nuke



# Text content
text = "FANCYPANTS"
# A separator for separating strings
splitKey = ""
# Font name(Not for the time being.)
fontFamily = "arial"
# Format name
formatName = "1K_Super_35(full-ap)"
# Font size
fontSize = 100
# letter-spacing
letterSpacing = 0
# word-spacing(Not for the time being.)
wordSpacing = 0
# line-height
lineHeight = 100
# Scene width and height
screenWidth = 0
screenHeight = 0
# Number of frames per character animated
stepLength = 10
offsetCountFrame = 8
# Segmented character array
textArr = []
if splitKey == "":
    textArr = list(text)
else:
    textArr = text.split(splitKey)
# Character array length
textArrLength = len(textArr)
# Total Frames
frameCount = textArrLength * stepLength


fontFamilyCss = "@font-face{font-family: '"+fontFamily+"';src: url('../nukeLibraries/fonts/"+fontFamily+".ttf') format('truetype');}"

letterSpacingNuke = letterSpacing * 1.0 / fontSize * 1;
wordSpacingNuke = wordSpacing * 1.0 / fontSize * 1;


chromeOptions = webdriver.ChromeOptions()
chromeOptions.add_argument("--headless")
browser = webdriver.Chrome(
    executable_path="C:\Users\FPG\AppData\Local\Programs\Python\Python37\Scripts\chromedriver.exe",
    options=chromeOptions)
# Create a local web server
browser.get('http://192.168.1.22:8080/Text3/index.html')

wait = WebDriverWait(browser, 10)
wait.until(EC.presence_of_all_elements_located((By.ID, "content")))


sum = 0
textContent = ""
textArrContent = []
for n in textArr:
    # if n == " ":
    #     n = "&nbsp;"
    n = n.replace(" ", "&nbsp;")
    if sum < textArrLength - 1:
        textContent += "<span>" + n + splitKey.replace(" ", "&nbsp;") + "</span>"
        textArrContent.append(n + splitKey)
    else:
        textContent += "<span>" + n + "</span>"
        textArrContent.append(n)
    sum += 1

time.sleep(1)

browser.execute_script(
    "calculationTextValue('" + textContent + "','" + str(fontSize) + "', '" + str(lineHeight) + "', '" + str(
        letterSpacing) + "', '" + str(wordSpacing) + "');")

html = BeautifulSoup(browser.page_source, features="html.parser")


characterWidth = html.select("#character-width")[0].text.split(",")
characterWidthArr = [int(i) for i in characterWidth]
textWidth = int(html.select("#text-width")[0].text)

characterHeight = html.select("#character-height")[0].text.split(",")
characterHeightArr = [int(i) for i in characterHeight]
textHeight = int(html.select("#text-height")[0].text)

print characterWidthArr
print textWidth
print characterHeightArr
print textHeight


nuke.root()["last_frame"].setValue(frameCount/2)
nuke.root()["format"].setValue(formatName)

for formatObj in nuke.formats():
    if formatObj.name() == formatName:
        screenWidth = formatObj.width()
        screenHeight = formatObj.height()
print screenWidth
print screenHeight

mergeNode = nuke.nodes.Merge2(name="Merge1")
count = 0
inputStep = 0
for s in textArrContent:
    textNode = nuke.nodes.Text2(name="Text"+str(count+1)+"")
    textNode["font"].setValue('Arial', 'Regular')
    textNode["opacity"].setValue(0)
    textNode["color"].setValue((0.020667, 0.318667, 0.620667, 1.0))
    textNode["box"].setValue((0, characterHeightArr[count]-textHeight, characterWidthArr[count], characterHeightArr[count]))
    textNode.knob("message").setValue(s)
    textNode.knob("xjustify").setValue("left")
    textNode.knob("yjustify").setValue("center")
    fontSizeValues = "{"
    kernValues = "{"
    for m in range(len(s)):
        kernValues += str(m) + " " + str(letterSpacingNuke) + " "
        fontSizeValues += str(m) + " " + str(fontSize) + " "
    kernValues += "}"
    fontSizeValues += "}"
    textNode.knob("kern_values").fromScript(kernValues)
    textNode.knob("kerning").toScript()
    textNode.knob("font_size_values").fromScript(fontSizeValues)
    textNode.knob("font_size").toScript()

    offsetCount = 0
    if count == 0:
        offsetCount = 0
    else:
        sum = 0
        for i in range(count):
            sum += characterWidthArr[i]
        offsetCount = sum


    transformNodeAnimate = nuke.nodes.Transform(name="TransformAnimate"+str(count+1)+"")
    transformNodeAnimate["translate"].setValue((offsetCount, -textHeight))
    transformNodeAnimate.setInput(0, textNode);

    kTransform = transformNodeAnimate["translate"]
    kTransform.setAnimated()

    kText = textNode["opacity"]
    kText.setAnimated()

    for f in range(textArrLength + 1):
        # if count < f:
        #     kTransform.setValueAt(offsetCount, f*stepLength, 0)
        #     kTransform.setValueAt(0, f*stepLength, 1)
        #     kText.setValueAt(1, f*stepLength)
        # else:
        #     kTransform.setValueAt(offsetCount, f*stepLength, 0)
        #     kTransform.setValueAt(-textHeight, f * stepLength, 1)
        #     kText.setValueAt(0, f*stepLength)
        if count == f:
            kTransform.setValueAt(offsetCount, f * stepLength-offsetCountFrame*count, 0)
            kTransform.setValueAt(-textHeight, f * stepLength-offsetCountFrame*count, 1)
            kText.setValueAt(0, f*stepLength-offsetCountFrame*count)
        if count + 1 == f:
            kTransform.setValueAt(offsetCount, f * stepLength-offsetCountFrame*count, 0)
            kTransform.setValueAt(0, f*stepLength-offsetCountFrame*count, 1)
            kText.setValueAt(1, f*stepLength-offsetCountFrame*count)

    for j in range(stepLength+1):
        kTransform.animation(1).setKey(stepLength*count-offsetCountFrame*count + j, -textHeight+ease.easeOutBack(j/10.0)*textHeight)
        kText.animation(0).setKey(stepLength*count-offsetCountFrame*count + j, ease.easeOutBack(j/10.0))


    # for key in kText.animation(0).keys():
    #     xValue = key.x
    #     yValue = key.y
    #     print 'key at %s has value %s' % (xValue, yValue)
    #
    # print kText.animations()

    # kText.animation(0).setExpression('curve(0)')
    # print kText.animation(0).keys()[0].y


    if count == 2:
        inputStep = 1
    mergeNode.setInput(count+inputStep, transformNodeAnimate)

    count += 1




transformNode = nuke.nodes.Transform(name="Transform"+str(count+1)+"")
#In order to prevent errors between browser and NUKE, each acquired width will increase the value of 2 pixels, so it is necessary to reduce the excess width when calculating the total width, because the height of each character will be increased by a certain height deviation (to prevent some characters from being truncated, such as the Y character), so the height will need to add a deviation value.
transformNode["translate"].setValue(((screenWidth-textWidth-2*textArrLength+letterSpacing)/2.0, (screenHeight-characterHeightArr[0] - (characterHeightArr[0]-textHeight))/2.0))
transformNode.setInput(0, mergeNode);

for j in range(1):
    writeNode = nuke.nodes.Write(name="Write1", file="C:/Users/FPG/Desktop/test"+str(j)+".mov", file_type="mov")
    writeNode.setInput(0, transformNode)

    nuke.execute(writeNode, 1, nuke.root().lastFrame(), j+1)








