
tn = nuke.thisNode()
nukeTargetNodesName = tn.knob("NukeTargetNodes").value()
nukeTargetMultipleAttrName = tn.knob("NukeTargetAttributeMultiple").value()
nukeTargetAttributeSingleName = tn.knob("NukeTargetAttributeSingle").value()

getJsonData(0)

def updateNukeTargetNodes():
    tn.knob("NukeTargetNodes").setValue(nukeTargetNodesName)

    targetAttributeMultiple = []
    targetAttributeSingle = []

    if nukeTargetNodesName != "None":
        for knob in nuke.toNode(nukeTargetNodesName).allKnobs():
            knobName = knob.name()
            if knobName != "":
                targetAttributeMultiple.append(knobName)
        targetAttributeMultiple.sort()
        targetAttributeMultiple.insert(0, "None")
        tn.knob("NukeTargetAttributeMultiple").setValues(targetAttributeMultiple)
    else:
        tn.knob("NukeTargetAttributeMultiple").setValues(["None"])
    tn.knob("NukeTargetAttributeMultiple").setValue(nukeTargetMultipleAttrName)

    if nukeTargetMultipleAttrName != "None":
        multipleAttrKnobs = nuke.toNode(nukeTargetNodesName).knob(nukeTargetMultipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.vect())):
                    targetAttributeSingle.append(multipleAttrKnobs.names(i))
                targetAttributeSingle.insert(0, "None")
                tn.knob("NukeTargetAttributeSingle").setValues(targetAttributeSingle)
            else:
                tn.knob("NukeTargetAttributeSingle").setValues(["None"])
        else:
            tn.knob("NukeTargetAttributeSingle").setValues(["None"])
    else:
        tn.knob("NukeTargetAttributeSingle").setValues(["None"])
    tn.knob("NukeTargetAttributeSingle").setValue(nukeTargetAttributeSingleName)
    #AEToNukeV1.getNukeTargetInfo
    getNukeTargetInfo()


updateNukeTargetNodes()
