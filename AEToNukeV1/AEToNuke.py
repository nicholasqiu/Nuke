#nuke.thisNode().knob("knobChanged").setValue('''


import json

tn = nuke.thisNode()
tk = nuke.thisKnob()

def setSinglePropValue(singleProp):
    tn.knob("AEProptypesSingle").setValues(singleProp)
    tn.knob("AEProptypesSingle").setValue(0)


def setAEPropValue(AEFirstFrame, AEFirstFrameValue, AELastFrame, AELastFrameValue):
    tn.knob("AEFirstFrame").setValue(AEFirstFrame)
    tn.knob("AEFirstFrameValue").setValue(AEFirstFrameValue)
    tn.knob("AELastFrame").setValue(AELastFrame)
    tn.knob("AELastFrameValue").setValue(AELastFrameValue)

if tk.name() == "AEData":
    fileContent = getJsonData(1)


if tk.name() == "AEProptypes":
    AEProptypes = tn.knob("AEProptypes").values()
    AEProptypeCurr = tn.knob("AEProptypes").value()

    singleProp = ["None"]
    if AEProptypeCurr == "None":
        singleProp = ["None"]
        tn.knob("AEFPS").setValue(0)
        tn.knob("AEScreenWidth").setValue(0)
        tn.knob("AEScreenHeight").setValue(0)
        tn.knob("AEState").setValue("信息选择不完整！！！")
        setAEPropValue(0, 0, 0, 0)
    else:
        tIndexArr = [i for i, name in enumerate(AEProptypes) if name == AEProptypeCurr]
        tIndex = 0
        if len(AEProptypes) > 1:
            tIndex = tIndexArr[0] - 1

        positionInfo = fileContent[2][tIndex]
        positionInfoArr = positionInfo.split("/")
        dataInfo = None
        objInfo = None
        for i in range(len(positionInfoArr)):
            if positionInfoArr[i].isdigit():
                if i == 0:
                    dataInfo = fileContent[0][int(positionInfoArr[i])]
                    tn.knob("AEFPS").setValue(float(dataInfo["fps"]))
                    tn.knob("AEScreenWidth").setValue(float(dataInfo["width"]))
                    tn.knob("AEScreenHeight").setValue(float(dataInfo["height"]))
                elif i == 1:
                    dataInfo = dataInfo["layers"][int(positionInfoArr[i])]
                else:
                    dataInfo = dataInfo["proptype"][int(positionInfoArr[i])]
            else:
                tn.knob("AEState").setValue("数据格式错误！！！")
                dataInfo = None
                break
        objInfo = dataInfo
        if dataInfo != None:
            dataInfo = dataInfo["data"]

            singleAttr = ["x", "y", "z"]
            if len(dataInfo[0]) > 1:
                for j in range(len(dataInfo[0])):
                    singleProp.append(singleAttr[j])
                setSinglePropValue(singleProp)
                setAEPropValue(0, 0, 0, 0)
                tn.knob("AEState").setValue("信息选择不完整！！！")
            else:
                singleProp = ["None"]
                setSinglePropValue(singleProp)
                setAEPropValue(int(objInfo["firstFrame"]), float(dataInfo[0][0]), int(objInfo["lastFrame"]), float(dataInfo[len(dataInfo) - 1][0]))
                tn.knob("AEState").setValue("ok")
        else:
            singleProp = ["None"]
            setSinglePropValue(singleProp)
            setAEPropValue(0, 0, 0, 0)



if tk.name() == "AEProptypesSingle":
    AEProptypesSingle = tn.knob("AEProptypesSingle").values()
    AEProptypesSingleCurr = tn.knob("AEProptypesSingle").value()
    tIndexArr = [i for i, name in enumerate(AEProptypesSingle) if name == AEProptypesSingleCurr]
    tIndex = 0
    if len(AEProptypesSingle) > 1:
        tIndex = tIndexArr[0] - 1

    if tk.value() == "None":
        setAEPropValue(0, 0, 0, 0)
        tn.knob("AEState").setValue("信息选择不完整！！！")
    else:
        setAEPropValue(int(objInfo["firstFrame"]), float(dataInfo[0][tIndex]), int(objInfo["lastFrame"]), float(dataInfo[len(dataInfo) - 1][tIndex]))
        tn.knob("AEState").setValue("ok")




tn.knob("NukeFirstFrame").setValue(tn.knob("AEFirstFrame").value())
tn.knob("NukeLastFrame").setValue(tn.knob("AELastFrame").value())






def chooiceNukeTargetInfo():
    targetNodeName = tn.knob("NukeTargetNodes").value()
    targetAttributeMultipleName = tn.knob("NukeTargetAttributeMultiple").value()
    targetAttributeSingleName = tn.knob("NukeTargetAttributeSingle").value()
    targetAttributeSingleNames = tn.knob("NukeTargetAttributeSingle").values()

    targetFirstFrame = tn.knob("NukeFirstFrame").value()
    targetLastFrame = tn.knob("NukeLastFrame").value()

    if targetNodeName != "None" and targetAttributeMultipleName != "None" and targetAttributeSingleName != "None":
        if targetLastFrame > targetFirstFrame:
            tn.knob("NukeState").setValue("ok")
        else:
            tn.knob("NukeState").setValue("信息填写不完整！！！")
    elif targetNodeName != "None" and targetAttributeMultipleName != "None" and len(targetAttributeSingleNames) <= 1:
        if targetLastFrame > targetFirstFrame:
            tn.knob("NukeState").setValue("ok")
        else:
            tn.knob("NukeState").setValue("信息填写不完整！！！")
    else:
        tn.knob("NukeState").setValue("信息填写不完整！！！")


def setNukeTargetInfo():
    pass
    # tn.knob("NukeFirstFrame").setValue(0)
    # tn.knob("NukeLastFrame").setValue(0)


def getNukeTargetInfo():
    tNodeName = tn.knob("NukeTargetNodes").value()
    tAttrMultipleName = tn.knob("NukeTargetAttributeMultiple").value()
    tAttrSingleName = tn.knob("NukeTargetAttributeSingle").value()
    tAttrSingleNames = tn.knob("NukeTargetAttributeSingle").values()

    if (tNodeName != "None" and tAttrMultipleName != "None" and tAttrSingleName != "None") or (tNodeName != "None" and tAttrMultipleName != "None" and len(tAttrSingleNames) <= 1):
        tIndexArr = [i for i, name in enumerate(tAttrSingleNames) if name == tAttrSingleName]
        tIndex = 0
        if len(tAttrSingleNames) > 1:
            tIndex = tIndexArr[0] - 1
        tAnimation = nuke.toNode(tNodeName).knob(tAttrMultipleName)

        if tAnimation.isAnimated() and (tAnimation.animation(tIndex) != None) and len(tAnimation.animation(tIndex).keys()) > 1:
            tAnimationCurve = tAnimation.animation(tIndex)
            tkeysCount = len(tAnimationCurve.keys())
            # tn.knob("NukeFirstFrame").setValue(tAnimationCurve.keys()[0].x)
            tn.knob("NukeStartingPosition").setValue(tAnimationCurve.keys()[0].y)
            # tn.knob("NukeLastFrame").setValue(tAnimationCurve.keys()[tkeysCount - 1].x)
        elif tAnimation.hasExpression() and tAnimation.hasExpression(tIndex):

            express = tAnimation.animation(tIndex).expression()

            count = 0
            index = 0
            for i in express:
                if count == 2:
                    break
                else:
                    if i == ":":
                        count += 1
                index += 1

            strArr = express[0:index - 2].replace("frame", "").replace("<", "").replace(">", "").replace(" ", "").split(
                ":")
            frameInfoArr = []
            for str in strArr:
                currStrArr = str.split("?")
                frameInfoArr.append(int(float(currStrArr[0])))
                frameInfoArr.append(float(currStrArr[1]))

            # tn.knob("NukeFirstFrame").setValue(frameInfoArr[0])
            tn.knob("NukeStartingPosition").setValue(frameInfoArr[1])
            # tn.knob("NukeLastFrame").setValue(frameInfoArr[2])
        else:
            setNukeTargetInfo()
    else:
        setNukeTargetInfo()


    chooiceNukeTargetInfo()



nukeAttributeMultiple = []
nukeAttributeSingle = []

if tk.name() == "NukeTargetNodes":
    nodeName = tk.value()
    if nodeName != "None":
        for knob in nuke.toNode(nodeName).allKnobs():
            knobName = knob.name()
            if knobName != "":
                nukeAttributeMultiple.append(knobName)
        nukeAttributeMultiple.sort()
        nukeAttributeMultiple.insert(0, "None")
        tn.knob("NukeTargetAttributeMultiple").setValues(nukeAttributeMultiple)
    else:
        tn.knob("NukeTargetAttributeMultiple").setValues(["None"])

    tn.knob("NukeTargetAttributeSingle").setValues(["None"])
    tn.knob("NukeTargetAttributeMultiple").setValue(0)
    tn.knob("NukeTargetAttributeSingle").setValue(0)

    getNukeTargetInfo()

if tk.name() == "NukeTargetAttributeMultiple":
    multipleAttrName = tk.value()
    if multipleAttrName != "None":
        nodeName = tn.knob("NukeTargetNodes").value()
        multipleAttrKnobs = nuke.toNode(nodeName).knob(multipleAttrName)
        if hasattr(multipleAttrKnobs, "vect") and hasattr(multipleAttrKnobs, "names"):
            if len(multipleAttrKnobs.vect()) > 1:
                for i in range(len(multipleAttrKnobs.vect())):
                    nukeAttributeSingle.append(multipleAttrKnobs.names(i))
                nukeAttributeSingle.insert(0, "None")
                tn.knob("NukeTargetAttributeSingle").setValues(nukeAttributeSingle)
            else:
                tn.knob("NukeTargetAttributeSingle").setValues(["None"])
        else:
            tn.knob("NukeTargetAttributeSingle").setValues(["None"])
    else:
        tn.knob("NukeTargetAttributeSingle").setValues(["None"])
    tn.knob("NukeTargetAttributeSingle").setValue(0)

    getNukeTargetInfo()

if tk.name() == "NukeTargetAttributeSingle":
    getNukeTargetInfo()


if tk.name() == "NukeFirstFrame"  or tk.name() == "NukeLastFrame"  or tk.name() == "NukeStartingPosition":
    chooiceNukeTargetInfo()




if tn.knob("AEState").value() == "ok" and tn.knob("NukeState").value() == "ok":
    tn.knob("NukeSetCurve").setEnabled(True)
else:
    tn.knob("NukeSetCurve").setEnabled(False)






























#''')
