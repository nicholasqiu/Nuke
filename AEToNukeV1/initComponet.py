# nuke.thisNode().knob("updateUI").setValue('''

import json

tn = nuke.thisNode()
tk = nuke.thisKnob()
allNodes = nuke.root().nodes()

def setEnabled():
    tn.knob("AEFPS").setEnabled(False)
    tn.knob("AEScreenWidth").setEnabled(False)
    tn.knob("AEScreenHeight").setEnabled(False)
    tn.knob("AEFirstFrame").setEnabled(False)
    tn.knob("AEFirstFrameValue").setEnabled(False)
    tn.knob("AELastFrame").setEnabled(False)
    tn.knob("AELastFrameValue").setEnabled(False)
    tn.knob("NukeFPS").setEnabled(False)
    tn.knob("NukeScreenWidth").setEnabled(False)
    tn.knob("NukeScreenHeight").setEnabled(False)

def getJsonData(type):
    oldAEProp = tn.knob("AEProptypes").values()
    fileContent = None
    path = tn.knob("AEData").value();
    with open(path) as file:
        fileContent = json.load(file)
    if isinstance(fileContent, list):
        pullDownArr = []
        for i in range(len(fileContent[1])):
            pullDownArr.append(fileContent[1][i].encode("utf-8"))

        # pullDownArr.sort()
        pullDownArr.insert(0, "None")

        ots = list(set(oldAEProp).difference(set(pullDownArr)))
        sto = list(set(pullDownArr).difference(set(oldAEProp)))

        if type == 0:
            tn.knob("AEProptypes").setValues(pullDownArr)
            tn.knob("AEProptypes").setValue(0)

            tn.knob("AEProptypesSingle").setValues(["None"])
            tn.knob("AEProptypesSingle").setValue(0)
            tn.knob("AEFPS").setValue(0)
            tn.knob("AEScreenWidth").setValue(0)
            tn.knob("AEScreenHeight").setValue(0)
            tn.knob("AEFirstFrame").setValue(0)
            tn.knob("AEFirstFrameValue").setValue(0)
            tn.knob("AELastFrame").setValue(0)
            tn.knob("AELastFrameValue").setValue(0)
            tn.knob("AEState").setValue("信息选择不完整！！！")
        else:
            if len(ots) > 0 or len(sto) > 0:
                tn.knob("AEProptypes").setValues(pullDownArr)
                tn.knob("AEProptypes").setValue(0)

                tn.knob("AEProptypesSingle").setValues(["None"])
                tn.knob("AEProptypesSingle").setValue(0)
                tn.knob("AEFPS").setValue(0)
                tn.knob("AEScreenWidth").setValue(0)
                tn.knob("AEScreenHeight").setValue(0)
                tn.knob("AEFirstFrame").setValue(0)
                tn.knob("AEFirstFrameValue").setValue(0)
                tn.knob("AELastFrame").setValue(0)
                tn.knob("AELastFrameValue").setValue(0)
                tn.knob("AEState").setValue("信息选择不完整！！！")
    else:
        tn.knob("AEState").setValue("文件类型不符合！！！")

    return fileContent

fileContent = getJsonData(1)

oldTargetNodes = tn.knob("NukeTargetNodes").values()
targetNodes = []

for node in allNodes:
    targetNodes.append(node.name())

targetNodes.sort()
targetNodes.insert(0, "None")

ots = list(set(oldTargetNodes).difference(set(targetNodes)))
sto = list(set(targetNodes).difference(set(oldTargetNodes)))

if len(ots) > 0 or len(sto) > 0:
    tn.knob("NukeTargetNodes").setValues(targetNodes)

tn.knob("NukeFPS").setValue(nuke.root().fps())
tn.knob("NukeScreenWidth").setValue(nuke.root()["format"].value().width())
tn.knob("NukeScreenHeight").setValue(nuke.root()["format"].value().height())


setEnabled()

# ''')
