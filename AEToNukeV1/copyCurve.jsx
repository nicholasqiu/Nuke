﻿var infoArr = [];
var dataInfo = [];
var inputDataInfo = [];
// var myComp = app.project.activeItem;
// 去掉左右空格
function trim(s) {
    return s.replace(/(^\s*)|(\s*$)/g, "");
}

function getInfo(layer, fps, currentProp, currentObj) {
    if (currentProp.numProperties && currentProp.numProperties > 0) {
        for (var i = 1; i <= currentProp.numProperties; i++) {
            var currProp = currentProp.property(i);
            getInfo(layer, fps, currProp, currentObj);
        }
    } else {

        var indexArrMatchName = [currentProp.matchName];
        var indexArrName = [currentProp.name];
        var indexArr = [currentProp.matchName];
        while(currentProp.parentProperty && currentProp.parentProperty.matchName){
            indexArrMatchName.unshift(currentProp.parentProperty.matchName);
            indexArrName.unshift(currentProp.parentProperty.name);
            indexArr.unshift(currentProp.parentProperty.matchName);
            currentProp = currentProp.parentProperty
        }
        indexArr[0] = indexArrName[0];
        var propCurrName = null;
        for (var i = 0; i < indexArrMatchName.length; i++){
            if(propCurrName === null){
                propCurrName = layer.property(indexArrMatchName[i])
            }else{
                propCurrName = propCurrName.property(indexArrMatchName[i])
            }
        }

        var numKeys = 0;
        if (propCurrName) {
            numKeys = propCurrName.numKeys;
        }
        // 关键帧的数量要大于等于2
        if (numKeys >= 2) {
            var dataObj = {
                "path": "",
                "pathName": "",
                "pathMatchName": "",
                "firstFrame": 0,
                "lastFrame": 0,
                "data": []
            };
            keyTimeFirst = propCurrName.keyTime(1);
            keyTimeLast = propCurrName.keyTime(numKeys);
            dataObj.path = indexArr.join("/");
            dataObj.pathName = indexArrName.join("/");
            dataObj.pathMatchName = indexArrMatchName.join("/");
            dataObj.firstFrame = Math.floor(keyTimeFirst * fps);
            dataObj.lastFrame = Math.ceil(keyTimeLast * fps);
            frameCount = dataObj.lastFrame - dataObj.firstFrame;
            for (var l = 0; l <= frameCount; l++) {
                dataObj.data.push(propCurrName.valueAtTime(l / fps, false).toString().split(','));
            }
            currentObj.push(dataObj);
        }
    }
}

function getAllInfo() {
    var infoArr = [];
    // 获取所有合成（项目）
    for (var n = 1; n <= app.project.numItems; n++) {
        var compInfo = {};
        var myComp = app.project.item(n);
        var fps = myComp.frameRate;
        compInfo.name = myComp.name;
        compInfo.matchName = myComp.name;
        compInfo.typeName = myComp.typeName;
        compInfo.fps = fps;
        compInfo.width = myComp.width;
        compInfo.height = myComp.height;
        compInfo.layers = [];
        if (fps !== undefined) {
            // 获取合成（项目）下的所有层
            for (var i = 1; i <= myComp.numLayers; i++) {
                var currLayer = myComp.layer(i);
                getInfo(currLayer, fps, currLayer, compInfo.layers);
                // compInfo.layers.push(layerInfo);
            }
            if(compInfo.layers.length > 0){
                infoArr.push(compInfo);
            }
        }
    }
    return infoArr;
}


function run() {
    var scriptName = "Copy Curve";
    var newLocation = Folder.selectDialog("Select a output folder...");
    if (newLocation != null) {
        app.beginUndoGroup(scriptName);
        infoArr = getAllInfo();
        var chartSys = "\\";
        if (File.fs == "Macintosh") {
            chartSys = "/";
        }
        var dataFile = new File(newLocation.toString() + chartSys + "data.json");
        dataFile.open("w");
        dataFile.encoding = "UTF-8";
        dataFile.write(JSON.stringify(infoArr));
        dataFile.close();
        alert("写入完成！！！");
        app.endUndoGroup();
    }
}

run();

/*
$.writeln(File.fullName);
$.writeln(File.fs);
$.writeln($.fileName);
$.writeln(File($.fileName).path)
*/